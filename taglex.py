'''
:Author: bipface
:Licence: `BSD-2-Clause <https://opensource.org/licenses/BSD-2-Clause>`_

A lexer (tokeniser) for the `Danbooru tag expression <helpCheatsheet_>`_
language used for searching and editing posts.

Basic Usage
===========

The :func:`tagExprTokens` function accepts an expression as a string
and returns an iterable of :class:`Tokens <Token>`.

>>> from taglex import tagExprTokens
>>> toks = list(tagExprTokens('re:zero (/ae or hat) is:sfw', mode='query'))
>>> [t.type for t in toks]
['literal', 'delim', 'open', 'abbrev', 'literal', 'delim', 'keyword', 'delim',
  'literal', 'close', 'delim', 'meta', 'literal']
>>> [t.value for t in toks]
['re:zero', ' ', '(', '/', 'ae', ' ', 'or', ' ', 'hat', ')', ' ', 'is:', 'sfw']

Lexer Overview
==============

The lexer simply separates an expression into a series of
:class:`Tokens <Token>`. See :const:`TokenType` for a summary of each
of the possible types of tokens.

A lexer is distinct from a parser - while the lexer
performs lexical analysis of an expression, it doesn't perform syntax analysis.
This approach has advantages and drawbacks, best described by comparison against
parsing:

.. csv-table::
  :header: "", "Lexer", "Parser"

  "Identify plain tags", "Yes", "Yes"
  "Identify wildcards in tags", "Yes", "Yes"
  "Identify wildcards in meta-values", "No", "Potentially"
  "Identify grouping parentheses", "Yes", "Yes"
  "Collate terms into groups/sub-groups", "No", "Yes"
  "Identify unary and infix operators", "Yes", "Yes"
  "Identify meta-prefixes", "Yes", "Yes"
  "Identify meta-values", "Yes", "Yes"
  "Identify meta-value quotes and escapes", "Yes", "Yes"
  "Simplify expressions", "No", "Potentially"
  "Collapse contiguous whitespace", "Yes", "Yes"
  "Casefolding/normalisation", "No", "Yes"
  "Validating tag names", "No", "Yes"
  "Error handling", "No errors.", "Expression is deemed invalid."

The lexer has a number of potential applications, some of which a parser may
not be suitable for:

* Syntax highlighting.
* Incremental editing of expressions - by knowing the exact offsets where terms
  begin and end, tokens may be deleted from or spliced into an expression
  without converting the entire syntax-tree back into a string.
* Scanning the expression until some condition is met (e.g. checking whether
  a term is present), which might be performed without having to read the
  entire expression.
* Providing a foundation for implementing a parser, with some of the
  non-trivial details already handled by the lexer.

However, some use cases can't be easily met by examining the lexer's output
and would be better served by a parser, such as:

* Determining whether a term is negated.
* Identifying conflicting or redundant terms.
* Determining whether an expression is valid.

Lexing is be a reversible operation - by default no information is discarded
and a sequence of tokens can be converted back into the original string.
In particular, this means token values aren't converted to lowercase
automatically.

.. seealso::

  `Danbooru's query-mode parser <parser_>`_

  `Danbooru's edit-mode parser <postEdit_>`_

Terms
=====

A term in the lexed expression may consist of one or more
tokens, and some types of tokens may not belong to any term.

Tag Terms
---------

A tag is a series of one or more contiguous literals and/or wildcard operators,
optionally prefixed with an abbreviation operator.

Token pattern for tag terms::

  [{abbrev}] ({literal} | {wildcard}) ...

A tag may contain any combination of literals and wildcards,
without contiguous tokens of the same type - a literal can't follow a literal,
and a wildcard can't follow a wildcard. Contiguous asterisks are lexed into
a single wildcard token:

>>> [t.type for t in tagExprTokens('a**b', mode='query')]
['literal', 'wildcard', 'literal']

Meta Terms
----------

A meta term (sometimes referred to as a 'metatag', confusingly)
consists of a meta-prefix followed by a value. The value may optionally be
surrounded with quotes, and may include escape sequences to place literal
spaces or quotes within the value.

Token patterns for meta terms::

  Empty value:
  {meta}
  {meta} {quote} {quote}

  Unquoted value:
  {meta} [{escape}] {literal} [({escape} {literal}) ...]

  Quoted value:
  {meta} {quote} [{escape}] {literal} [({escape} {literal}) ...] {quote}

  Quoted value (unclosed):
  {meta} {quote}
  {meta} {quote} [{escape}] {literal} [({escape} {literal}) ...]

Other Tokens
============

Non-ASCII Literals
------------------

One less-than-obvious detail in the language is that 'tags' containing
non-ASCII codepoints are handled specially - wildcard and abbreviation operators
occuring in such terms are not recognised and instead treated as part of the
literal.

In practice such terms are treated similar to aliases and handled by
performing an *other names* lookup.

The :func:`operateNonAsciiTagTokens` transducer is available to help
deal with non-ASCII terms.

.. seealso::

  `Danbooru's tag_other_name_matches code-path <otherNameMatches_>`_

Lexical Modes
=============

The language comes in two flavours: edit-mode and query-mode.
All lexing functions require an argument specifying which mode to operate in,
and when lexing incrementally it is an error to switch modes between
lexer invocations.

Query-mode recognises the full language. It is used for searching and
autocompletion. In Danbooru, this mode is implemented by `parser.rb <parser_>`_.

Edit-mode supports a reduced set of features and is used wherever
the ``tag_string`` of a post can be edited, including via
the *tag script* feature. In edit mode, the unary *or* operator (``~``),
wildcards, parenthesised groups and keywords are not recognised.
In Danbooru, this mode is implemented by `post_edit.rb <parser_>`_.

The Lexing Process
==================

The lexer operates as a state-machine:

* Based on the current state, it selects a set of transitions each consisting
  of a pattern to match against and a handler to be invoked upon matching.
* The input is applied to each of the patterns in turn. The transition handler
  for the first successful match is invoked.
* The handler returns the matched token and the new state.

:func:`tagExprNextToken` may be used to operate the lexer directly one
state-transition at a time. :func:`tagExprTokens` providers a higher-level
interface to lex an entire expression into multiple tokens at once.

The lexer could be easily ported to other languages with a regular expression
engine supporting lookahead-assertions, named capture-groups and non-greedy
match operators.

Because the lexer's patterns require unbounded lookahead, and due to Python's
regular expression library only supporting strings and byte-arrays, it's not
feasible to implement the lexer to read generic character iterables.

Transducers
===========

A number of token transducer functions are provided for convenience.
They may be used to further process a series of tokens into more friendly
formats.

* :func:`casefoldTokens`
* :func:`simplifyMetaTokens`
* :func:`literaliseWildcardTokens`
* :func:`literaliseAbbrevTokens`
* :func:`operateNonAsciiTagTokens`

Care must be taken to apply the transducers in a valid order. For example,
if :func:`simplifyMetaTokens` was used before :func:`casefoldTokens`, the values
of all meta terms would be lowercased, which may lead to a loss of information
with case-sensitive terms such as ``source:`` and ``newpool:``:

>>> [t.value for t in casefoldTokens(simplifyMetaTokens(
...   tagExprTokens('newpool:"Slice of Life"', mode='edit')))]
['newpool:slice of life']

>>> [t.value for t in simplifyMetaTokens(casefoldTokens(
...   tagExprTokens('newpool:"Slice of Life"', mode='edit')))]
['newpool:Slice of Life']

Issues
======

Please report bugs and proposals to `the GitLab project <issues_>`_.

.. _issues: https://gitlab.com/bipface/taglex/-/issues
.. _helpCheatsheet: https://danbooru.donmai.us/wiki_pages/help:cheatsheet
.. _parser: https://github.com/danbooru/danbooru/blob/master/app/logical/post_query/parser.rb
.. _postEdit: https://github.com/danbooru/danbooru/blob/master/app/logical/post_edit.rb
.. _otherNameMatches: https://github.com/danbooru/danbooru/blob/72528bdcb1fafd7d40b268dc5be33387a462051c/app/logical/autocomplete_service.rb#L114
'''

__version__ = '2022.10.22'

import re
from pprint import pformat
from re import Pattern, escape as reEsc
import functools
from functools import partial
from typing import NamedTuple, Literal, Optional, Union, Callable, cast
from collections.abc import Iterable, Mapping, Sequence

metaPrefixesForEditMode = (
	'art', 'artist', 'ch', 'char', 'character', 'child', 'child', 'co', 'copy',
	'copyright', 'disapproved', 'downvote', 'fav', 'fav', 'favgroup',
	'favgroup', 'gen', 'general', 'meta', 'newpool', 'parent', 'parent', 'pool',
	'pool', 'rating', 'source', 'status', 'status', 'upvote',)
'''
Set of meta-prefixes recognised when lexing in edit mode.
Obtained with ``PostEdit::METATAGS.map{|x| x.delete_prefix('-')}.sort``
in the Rails console.
'''
	
metaPrefixesForQueryMode = (
	'active_child_count', 'active_children', 'active_comment_count',
	'active_comments', 'active_note_count', 'active_notes', 'active_pool_count',
	'active_pools', 'age', 'ai', 'appeal_count', 'appealer', 'appeals',
	'approval_count', 'approvals', 'approver', 'artcomm', 'arttags', 'chartags',
	'child', 'child_count', 'children', 'collection_pool_count',
	'collection_pools', 'comm', 'comment', 'comment_count', 'commentary',
	'commentaryupdater', 'commenter', 'comments', 'copytags', 'date',
	'deleted_child_count', 'deleted_children', 'deleted_comment_count',
	'deleted_comments', 'deleted_note_count', 'deleted_notes',
	'deleted_pool_count', 'deleted_pools', 'disapproved', 'downvote',
	'downvotes', 'duration', 'embedded', 'exif', 'fav', 'favcount', 'favgroup',
	'filesize', 'filetype', 'flag_count', 'flagger', 'flags', 'gentags', 'has',
	'height', 'id', 'is', 'limit', 'md5', 'metatags', 'mpixels', 'note',
	'note_count', 'noter', 'notes', 'noteupdater', 'order', 'ordfav',
	'ordfavgroup', 'ordpool', 'parent', 'pixiv', 'pixiv_id', 'pool',
	'pool_count', 'pools', 'random', 'rating', 'ratio', 'replacement_count',
	'replacements', 'score', 'search', 'series_pool_count', 'series_pools',
	'source', 'status', 'tagcount', 'unaliased', 'upvote', 'upvotes', 'user',
	'width',)
'''
Set of meta-prefixes recognised when lexing in query mode.
Obtained with ``PostQueryBuilder::METATAGS.sort`` in the Rails console.
'''

permittedUnbalancedTagNames = (':)', ':(', ';)', ';(', '>:)', '>:(',)
'''
Tags that are permitted to have unbalanced parentheses, as a special exception
to the normal rule that parentheses in tags must balanced.

See `tag.rb#L9 <unbalNames_>`_

.. _unbalNames: https://github.com/danbooru/danbooru/blob/0cfd0ff436b30d7eaa0c05c72d088c97a8b47113/app/models/tag.rb#L9
'''

wildcardChr = '*'
escapeChr = '\\'
unaryNot = '-'
unaryOr = '~'
abbrevChr = '/'
keywordAnd = 'and'
keywordOr = 'or'
quoteSingle = '\''
quoteDouble = '"'
metaPrefixSep = ':'
groupOpen = '('
groupClose = ')'

_reNonAscii = r'\U00000080-\U0010ffff'
'''Range covering all non-ascii codepoints.'''

LexMode = Literal['query', 'edit']

TokenType = Literal[
	'delim',
	'literal',
	'abbrev', # /
	'wildcard', # *
	'unary', # -, ~
	'keyword', # and, or
	'open', # (
	'close', # )
	'meta', # xyz:
	'quote', # ', "
	'escape', # \
]
'''
Set of categories a token may belong to.

* ``delim`` - One or more contiguous whitespace characters between other tokens.
* ``literal`` - Segment of a tag name or meta-value.
* ``abbrev`` - Tag abbreviation operator before a tag name,
  e.g. the first character in ``/ltts``.
* ``wildcard`` - Asterisk within a tag name,
  e.g. the first character in ``*mark``.
* ``unary`` - Logical operator before a token,
  e.g. the first character in ``-skirt`` or ``~skirt``.
* ``keyword`` - Infix operator (``and``, ``or``).
* ``open`` - Parenthesis opening a group,
  e.g. the first character in ``(skirt or hat)``.
* ``close`` - Parenthesis closing a group,
  e.g. the last character in ``(skirt or hat)``.
* ``meta`` - Meta prefix, e.g. ``is:`` in ``is:sfw``.
* ``quote`` - The opening or closing quote character in a quoted meta-value.
* ``escape`` - Escape character in a meta-value,
  e.g. the backslash character in ``source:'Rin\\'s blog'``.
'''

class Token(NamedTuple):
	type: TokenType
	'''Category of token - see :const:`TokenType`.'''

	value: str = ''
	'''
	Substring from the expression where this token was read.
	By default, ``token.value == expr[token.start:token.end]``.
	'''

	start: int = 0
	'''Offset in the expression where the token begins.'''

	end: int = 0
	'''Offset in the expression where the token ends.'''

_StateName = Literal[
	'default',
	'unary',
	'abbrev',
	'metaprefix',
	'literal',
	'metavalue',
	'escape',
]

class LexState(NamedTuple):
	name: _StateName = 'default'
	'''Indicates the general context of the most-recently read token.'''

	nOpen: int = 0
	'''
	Number of currently-open groups (i.e. unbalanced parentheses).
	Can't be negative.
	'''

	nLiteralOpen: int = 0
	'''
	Number of unbalanced parentheses in the current literal sequence.
	A negative value indicates that parenthesis-counting is disabled for this
	literal sequence.
	Only used in the ``literal`` state.
	'''

	quote: str = ''
	'''
	Currently-open quote when in a meta-value. Must be
	one of :const:`quoteSingle`, :const:`quoteDouble` or an empty string.
	Only used in the ``metavalue`` and ``escape`` states.
	'''

def tagExprTokens(expr: str, mode: LexMode, start: int = 0) -> Iterable[Token]:
	'''
	Convenience function which invokes :func:`tagExprNextToken` repeatedly
	to lex *expr* starting from offset *start*,
	returning an iterable of :class:`Tokens <Token>`.
	'''

	state: Optional[LexState] = None
	while True:
		tok, state = tagExprNextToken(expr, mode, start, state)
		if tok is None:
			break

		assert expr.startswith(tok.value, start)
		assert tok.end == tok.start + len(tok.value)

		yield tok
		start = tok.end

def tagExprNextToken(expr: str, mode: LexMode, start: int = 0,
	state: Optional[LexState] = None
	) -> tuple[Optional[Token], LexState]:
	'''
	Given a *mode* and optionally *state*, reads the first :class:`Token`
	in *expr* starting from offset *start*.

	:param expr: The expression to read.
	:param mode: Specifies which lexical mode to operate in.
	:param start: Position in *expr* to start reading from.
	  The resulting token will have its ``start`` field set to this value.
	  Defaults to ``0``.
	:param state: The lexer state, or ``None`` if reading from the beginning of
	  the expression. As the lexer advances, the state object returned from
	  one invocation should be passed as an argument to the next invocation.

	:return: A :class:`Token` and the new lexer state. The token may be ``None``
	  if the expression was empty.
	'''

	state = state or LexState('default', 0)

	#import logging
	#logging.debug('Initial state: %s, expr: %s.',
		#pformat(state), pformat(expr[start:]))

	if len(expr) <= start:
		# Input is empty.
		return (None, state)

	patt, callbacks = _getCompiledTransitions(mode, state)

	m = patt.match(expr, start)

	if not m or not m.lastgroup:
		# This shouldn't happen - either the input isn't consistent between
		# transitions, or there's a bug in the transition patterns.
		raise ValueError(
			'''Expression doesn't match any group - '''
			f'''state: {pformat(state)}, expr: {pformat(expr)}.''')

	fn = callbacks[int(m.lastgroup.removeprefix('p'))]

	tok, newState = fn(m.group(m.lastgroup))

	#logging.debug('Result state: %s, tok: %s.', pformat(rv[1]), pformat(rv[0]))

	return (
		tok._replace(
			start=start,
			end=(start + len(tok.value))),
		newState)

_TrxnHdlr = Callable[[str], tuple[Token, LexState]]
'''
A transition handler takes the segment of the expression which satisfied
transition's pattern, and returns a token along with the new lexer state.
'''

def _trxnHdlr(tokType: TokenType, newState: LexState):
	'''
	Return a simple transition handler with a fixed token type and
	resulting state - for cases where the new state doesn't depend
	on the value of the token.
	'''
	return lambda m: (Token(tokType, m), newState)

@functools.cache
def _getCompiledTransitions(mode: LexMode, state: LexState
	) -> tuple[Pattern[str], Sequence[_TrxnHdlr]]:
	'''
	Given a *mode* and *state*, selects a set of transition definitions
	and compiles them into a single regular expression pattern, plus a
	series of transition handler functions to be invoked depending on
	which sub-pattern is matched.
	'''

	if mode == 'query':
		defs = _getTransitionDefsForQueryMode(state)
	elif mode == 'edit':
		defs = _getTransitionDefsForEditMode(state)
	else:
		raise ValueError(f'Unrecognised mode {pformat(mode)}.')

	allPatt = re.compile(
		'|'.join(f'(?P<p{i}>{patt})' for i, (patt, _) in enumerate(defs)))

	handlers = tuple(fn for i, (_, fn) in enumerate(defs))

	return (allPatt, handlers)

def _getTransitionDefsForEditMode(state: LexState
	) -> Sequence[tuple[str, _TrxnHdlr]]:
	'''
	Edit-mode uses a simpler syntax which doesn't include wildcards, groups,
	keywords or unary OR (``~``).
	'''

	if state.name == 'default':
		return (
			_delimTrxnDef(state), # Space
			_metaPrefixTrxnDef(state, metaPrefixesForEditMode),

			# Unary operator:
			(reEsc(unaryNot),
				_trxnHdlr('unary', state._replace(name='unary'))),

			# Abbrev operator before an ascii literal or delim:
			_abbrevTrxnDef(state),

			# Literal before a delim:
			(r'[^\s]+',
				_trxnHdlr('literal', state._replace(name='default'))),
		)

	if state.name == 'unary':
		return (
			_delimTrxnDef(state), # Space
			_metaPrefixTrxnDef(state, metaPrefixesForEditMode),

			# Abbrev operator before an ascii literal or delim:
			_abbrevTrxnDef(state),

			# Literal before a delim:
			(r'[^\s]+',
				_trxnHdlr('literal', state._replace(name='default'))),
		)

	if state.name == 'abbrev':
		return (
			# Space:
			_delimTrxnDef(state),

			# Ascii literal:
			(f'[^\\s{_reNonAscii}]+(?=\\s|\\Z)',
				_trxnHdlr('literal', state._replace(name='default'))),
		)

	if state.name == 'metaprefix':
		return (
			_delimTrxnDef(state), # Space following an empty meta value
			_quoteOpenTrxnDef(state), # Quote

			# Escape before a space:
			(f'{reEsc(escapeChr)}(?=\\s)',
				_trxnHdlr('escape', state._replace(name='escape', quote=''))),

			# Literal before an escaped space:
			(f'[^\\s]+?(?={reEsc(escapeChr)}\\s)',
				_trxnHdlr('literal',
					state._replace(name='metavalue', quote=''))),

			# Literal before a delim:
			(f'[^\\s]+',
				_trxnHdlr('literal',
					state._replace(name='metavalue', quote=''))),
		)

	if state.name == 'metavalue' and not state.quote:
		return (
			_delimTrxnDef(state), # Space following an empty meta value

			# Escape before a space:
			(f'{reEsc(escapeChr)}(?=\\s)',
				_trxnHdlr('escape', state._replace(name='escape'))),

			# Literal before an escaped space:
			(f'[^\\s]+?(?={reEsc(escapeChr)}\\s)',
				_trxnHdlr('literal', state._replace(name='metavalue'))),

			# Literal before a delim:
			(f'[^\\s]+',
				_trxnHdlr('literal', state._replace(name='metavalue'))),
		)
		
	if state.name == 'metavalue' and state.quote:
		equot = reEsc(state.quote)
		return (
			_quoteCloseTrxnDef(state), # Quote

			# Escape before a quote:
			(f'{reEsc(escapeChr)}(?={equot})',
				_trxnHdlr('escape', state._replace(name='escape'))),

			# Literal before an escaped quote:
			(f'[^{equot}]+?(?={reEsc(escapeChr)}{equot})',
				_trxnHdlr('literal', state._replace(name='metavalue'))),

			# Literal:
			(f'[^{equot}]+',
				_trxnHdlr('literal', state._replace(name='metavalue'))),
		)

	if state.name == 'escape' and not state.quote:
		return (
			# Literal (beginning with a space) before another escaped space:
			(f'\\s[^\\s]*?(?={reEsc(escapeChr)}\\s)',
				_trxnHdlr('literal', state._replace(name='metavalue'))),

			# Literal (beginning with a space):
			(f'\\s[^\\s]*',
				_trxnHdlr('literal', state._replace(name='metavalue'))),
		)

	if state.name == 'escape' and state.quote:
		equot = reEsc(state.quote)
		return (
			# Literal (beginning with a quote) before another escaped quote:
			(f'{equot}[^{equot}]*?(?={reEsc(escapeChr)}{equot})',
				_trxnHdlr('literal', state._replace(name='metavalue'))),

			# Literal (beginning with a quote):
			(f'{equot}[^{equot}]*',
				_trxnHdlr('literal', state._replace(name='metavalue'))),
		)

	assert False, f'No transitions defined for state {pformat(state)}.'

def _getTransitionDefsForQueryMode(state: LexState
	) -> Sequence[tuple[str, _TrxnHdlr]]:
	'''
	Query-mode supports the full language.
	'''

	if state.name == 'default' and state.nOpen == 0:
		return (
			_delimTrxnDef(state), # Space
			_metaPrefixTrxnDef(state, metaPrefixesForQueryMode),
			_keywordTrxnDef(state), # Keyword before delim

			# Unary operator:
			(f'[{reEsc(unaryOr)}{reEsc(unaryNot)}]',
				_trxnHdlr('unary', state._replace(name='unary'))),

			# Opening parenthesis:
			(reEsc(groupOpen),
				_trxnHdlr('open', state._replace(nOpen=(state.nOpen + 1)))),

			# Stray closing parenthesis:
			(reEsc(groupClose),
				_trxnHdlr('close', state._replace(name='default'))),

			# Abbrev operator before ascii*, before delim:
			_abbrevTrxnDef(state),

			# Wildcard before ascii*, before delim:
			_wildcardTrxnDef(state._replace(nLiteralOpen=0)),

			# Ascii literal before a wildcard before ascii*, before delim
			# (doesn't begin with a parenthesis or wildcard):
			_literalBeforeWildcardTrxnDef(state._replace(nLiteralOpen=0)),

			# Literal before a delim
			# (doesn't begin with a parenthesis or unary operator):
			_literalFallbackTrxnDef(state),
		)

	if state.name == 'default' and state.nOpen > 0:
		return (
			_delimTrxnDef(state), # Space
			_metaPrefixTrxnDef(state, metaPrefixesForQueryMode),
			_keywordTrxnDef(state), # Keyword before delim

			# Closing parenthesis:
			(reEsc(groupClose),
				_trxnHdlr('close', state._replace(nOpen=(state.nOpen - 1)))),

			# Unary operator:
			(f'[{reEsc(unaryOr)}{reEsc(unaryNot)}]',
				_trxnHdlr('unary', state._replace(name='unary'))),

			# Opening parenthesis:
			(reEsc(groupOpen),
				_trxnHdlr('open', state._replace(nOpen=(state.nOpen + 1)))),

			# Abbrev operator before ascii*, before delim:
			_abbrevTrxnDef(state),

			# Wildcard before ascii*, before delim:
			_wildcardTrxnDef(state._replace(nLiteralOpen=0)),

			# Ascii literal before a wildcard before ascii*, before delim
			# (doesn't begin with a parenthesis or wildcard):
			_literalBeforeWildcardTrxnDef(state._replace(nLiteralOpen=0)),

			# Literal among ``permittedUnbalancedTagNames`` before a delim:
			(f'''({'|'.join(map(reEsc, permittedUnbalancedTagNames))})(?=\\s|\\Z)''',
				_trxnHdlr('literal',
					state._replace(name='literal', nLiteralOpen=-1))),

			# Literal ending in closing parenthesis before a delim
			# (doesn't begin with a parenthesis):
			_closingLiteralTrxnDef(state._replace(nLiteralOpen=0)),
			
			# Literal before a delim (doesn't begin with a parenthesis,
			# doesn't end with closing parenthesis):
			_literalFallbackTrxnDef(state),
		)

	if state.name == 'unary' and state.nOpen == 0:
		return (
			_delimTrxnDef(state), # Space after stray operator
			_metaPrefixTrxnDef(state, metaPrefixesForQueryMode),

			# Opening parenthesis:
			(reEsc(groupOpen),
				_trxnHdlr('open', state._replace(
					name='default', nOpen=(state.nOpen + 1)))),

			# Stray closing parenthesis after stray operator:
			(reEsc(groupClose),
				_trxnHdlr('close', state._replace(name='default'))),

			# Abbrev operator before ascii*, before delim:
			_abbrevTrxnDef(state),

			# Wildcard before ascii*, before delim:
			_wildcardTrxnDef(state._replace(nLiteralOpen=0)),

			# Ascii literal before wildcard before ascii*, before delim
			# (doesn't begin with a parenthesis or wildcard):
			_literalBeforeWildcardTrxnDef(state._replace(nLiteralOpen=0)),

			# Literal before delim
			# (doesn't begin with a parenthesis):
			_literalFallbackTrxnDef(state),
		)

	if state.name == 'unary' and state.nOpen > 0:
		return (
			_delimTrxnDef(state), # Space after stray operator
			_metaPrefixTrxnDef(state, metaPrefixesForQueryMode),

			# Opening parenthesis:
			(reEsc(groupOpen),
				_trxnHdlr('open', state._replace(
					name='default', nOpen=(state.nOpen + 1)))),

			# Closing parenthesis after stray operator:
			(reEsc(groupClose),
				_trxnHdlr('close', state._replace(
					name='default', nOpen=(state.nOpen - 1)))),

			# Abbrev operator before ascii*, before delim:
			_abbrevTrxnDef(state),

			# Wildcard before ascii*, before delim:
			_wildcardTrxnDef(state._replace(nLiteralOpen=0)),

			# Ascii literal before a wildcard before ascii*, before delim
			# (doesn't begin with a parenthesis or wildcard):
			_literalBeforeWildcardTrxnDef(state._replace(nLiteralOpen=0)),

			# Literal among ``permittedUnbalancedTagNames`` before a delim:
			(f'''({'|'.join(map(reEsc, permittedUnbalancedTagNames))})(?=\\s|\\Z)''',
				_trxnHdlr('literal',
					state._replace(name='literal', nLiteralOpen=-1))),

			# Literal ending in closing parenthesis before a delim
			# (doesn't begin with a parenthesis):
			_closingLiteralTrxnDef(state._replace(nLiteralOpen=0)),
			
			# Literal before a delim (doesn't begin with a parenthesis
			# or wildcard):
			_literalFallbackTrxnDef(state),
		)

	if state.name == 'abbrev' and state.nOpen == 0:
		# The characters until the next delim are guaranteed to be ascii.

		return (
			_delimTrxnDef(state), # Space
			_wildcardTrxnDef(state._replace(nLiteralOpen=0)), # Wildcard

			# Literal before wildcard:
			_literalBeforeWildcardTrxnDef(state._replace(nLiteralOpen=0)),

			# Literal before delim:
			(f'[^\\s]+',
				_trxnHdlr('literal',
					state._replace(name='literal', nLiteralOpen=-1))),
		)

	if state.name == 'abbrev' and state.nOpen > 0:
		# The characters until the next delim are guaranteed to be ascii.
	
		return (
			_delimTrxnDef(state), # Space
			_wildcardTrxnDef(state._replace(nLiteralOpen=0)), # Wildcard

			# Literal before a wildcard:
			_literalBeforeWildcardTrxnDef(state._replace(nLiteralOpen=0)),

			# Literal not ending in a closing parenthesis, before a delim:
			_nonClosingLiteralTrxnDef(state),

			# Closing parentheses before less than ``nOpen`` closing
			# parentheses, before a delim:
			_multipleGroupCloseTrxnDef(state),

			# Literal before up to ``nOpen`` closing parentheses, before a delim:
			(f'[^\\s{reEsc(wildcardChr)}]+?(?={reEsc(groupClose)}{{1,{state.nOpen}}}(\\s|\\Z))',
				_trxnHdlr('literal', state._replace(name='literal', nLiteralOpen=0))),
		)

	if state.name == 'literal' and state.nOpen == 0:
		return (
			_delimTrxnDef(state), # Space
			_wildcardTrxnDef(state), # Wildcard
			
			# Literal before a wildcard:
			_literalBeforeWildcardTrxnDef(state),

			# Literal before a delim:
			(f'.[^\\s{reEsc(wildcardChr)}]*(?=\\s|\\Z)',
				_trxnHdlr('literal', state._replace(nLiteralOpen=-1))),
		)

	if state.name == 'literal' and state.nOpen > 0 and state.nLiteralOpen < 0:
		'''
		A term with a stray closing parenthesis in a previous literal,
		e.g. matching ``b`` in ``a)*b``. No matter how many closing parentheses
		are trailing in this literal the term will never be considered balanced,
		so all the trailing closing parentheses may be used to close groups.

		``nLiteralOpen`` might also be <0 if the literal is known to have
		already ended.
		'''

		return (
			_delimTrxnDef(state), # Space
			_wildcardTrxnDef(state), # Wildcard

			# Closing parentheses before less than ``nOpen`` closing
			# parentheses, before a delim:
			_multipleGroupCloseTrxnDef(state),

			# Literal before a wildcard:
			_literalBeforeWildcardTrxnDef(state),

			# Literal not ending in a closing parenthesis, before a delim:
			_nonClosingLiteralTrxnDef(state),

			# Literal before up to ``nOpen`` closing parentheses,
			# before a delim:
			(f'[^\\s{reEsc(wildcardChr)}]+?(?={reEsc(groupClose)}{{1,{state.nOpen}}}(\\s|\\Z))',
				_trxnHdlr('literal', state._replace(nLiteralOpen=0))),
		)

	if state.name == 'literal' and state.nOpen > 0 and state.nLiteralOpen > 0:
		'''
		A term where parentheses are left open from a previous literal,
		e.g. matching ``b`` in ``a(*b``. Trailing closing parentheses
		will first be used to balance the term, and any remaining may be used
		to close groups. If there aren't enough trailing closing parentheses
		to balance the term, they all may be used to close groups instead.
		'''

		ecl = reEsc(groupClose)
		return (
			_delimTrxnDef(state), # Space
			_wildcardTrxnDef(state), # Wildcard

			# Literal before a wildcard:
			_literalBeforeWildcardTrxnDef(state),

			# Literal not ending in a closing parenthesis, before a delim:
			_nonClosingLiteralTrxnDef(state),

			# Up to ``nLiteralOpen`` closing parentheses before a delim:
			(f'{ecl}{{1,{state.nLiteralOpen}}}(?=\\s|\\Z)',
				_trxnHdlr('literal', state._replace(nLiteralOpen=-1))),

			# Exactly ``nLiteralOpen`` closing parentheses before
			# up to ``nOpen`` closing parentheses, before a delim:
			(f'{ecl}{{{state.nLiteralOpen}}}(?={ecl}{{0,{state.nOpen}}}(\\s|\\Z))',
				_trxnHdlr('literal', state._replace(nLiteralOpen=-1))),

			# More than``nLiteralOpen`` closing parentheses before
			# exactly ``nOpen`` closing parentheses, before a delim:
			(f'{ecl}{{{state.nLiteralOpen},}}(?={ecl}{{{state.nOpen}}}(\\s|\\Z))',
				_trxnHdlr('literal', state._replace(nLiteralOpen=-1))),

			# Literal ending in closing parenthesis, before a delim:
			_closingLiteralTrxnDef(state),
		)

	if state.name == 'literal' and state.nOpen > 0 and state.nLiteralOpen == 0:
		return (
			_delimTrxnDef(state), # Space
			_wildcardTrxnDef(state), # Wildcard

			# Closing parentheses before less than ``nOpen`` closing
			# parentheses, before a delim:
			_multipleGroupCloseTrxnDef(state),

			# Literal before a wildcard:
			_literalBeforeWildcardTrxnDef(state),

			# Literal not ending in a closing parenthesis, before a delim:
			_nonClosingLiteralTrxnDef(state),

			# One or more closing parentheses before one to ``nOpen`` closing
			# parentheses, before a delim:
			(f'{reEsc(groupClose)}+?(?={reEsc(groupClose)}{{1,{state.nOpen}}}(\\s|\\Z))',
				_trxnHdlr('literal', state)),

			# Literal ending in closing parenthesis, before a delim:
			_closingLiteralTrxnDef(state),
		)

	if state.name == 'metaprefix' and state.nOpen == 0:
		return (
			_delimTrxnDef(state), # Space following an empty meta value
			_quoteOpenTrxnDef(state), # Quote

			# A literal before a delim:
			(f'[^\\s]+',
				_trxnHdlr('literal',
					state._replace(name='metavalue', quote=''))),
		)

	if state.name == 'metaprefix' and state.nOpen > 0:
		return (
			_delimTrxnDef(state), # Space following an empty meta value
			_quoteOpenTrxnDef(state), # Quote

			# Literal before 0 to ``nOpen`` closing parentheses, before a delim:
			(f'[^\\s]+?(?={reEsc(groupClose)}{{0,{state.nOpen}}}(\\s|\\Z))',
				_trxnHdlr('literal',
					state._replace(name='metavalue', quote=''))),

			# Closing parenthesis before less than ``nOpen`` closing
			# parentheses, before a delim:
			_multipleGroupCloseTrxnDef(state),
		)

	if state.name == 'metavalue' and state.nOpen == 0 and not state.quote:
		return (
			_delimTrxnDef(state), # Space

			# Literal before a delim:
			(f'[^\\s]+',
				_trxnHdlr('literal', state._replace(name='metavalue'))),
		)

	if state.name == 'metavalue' and state.nOpen > 0 and not state.quote:
		return (
			_delimTrxnDef(state), # Space

			# A sequence of up to ``nOpen`` closing parentheses before a delim:
			_multipleGroupCloseTrxnDef(state),

			# Literal before up to ``nOpen`` closing parentheses,
			# before a delim:
			(f'[^\\s]+?(?={reEsc(groupClose)}{{0,{state.nOpen}}}(\\s|\\Z))',
				_trxnHdlr('literal', state._replace(name='metavalue'))),
		)

	if state.name == 'metavalue' and state.quote:
		equot = reEsc(state.quote)
		return (
			_quoteCloseTrxnDef(state), # Quote

			# Escape before a quote:
			(f'{reEsc(escapeChr)}(?={equot})',
				_trxnHdlr('escape', state._replace(name='escape'))),

			# Literal before an escaped quote:
			(f'[^{equot}]+?(?={reEsc(escapeChr)}{equot})',
				_trxnHdlr('literal', state)),

			# Literal:
			(f'[^{equot}]+',
				_trxnHdlr('literal', state)),
		)

	if state.name == 'escape' and state.quote:
		equot = reEsc(state.quote)
		return (
			# Literal (beginning with a quote) before another escaped quote:
			(f'{equot}[^{equot}]*?(?={reEsc(escapeChr)}{equot})',
				_trxnHdlr('literal', state._replace(name='metavalue'))),

			# Literal (beginning with a quote):
			(f'{equot}[^{equot}]*',
				_trxnHdlr('literal', state._replace(name='metavalue'))),
		)

	assert False, f'No transitions defined for state {pformat(state)}.'

#region shared transition definitions	

def _delimTrxnDef(state: LexState) -> tuple[str, _TrxnHdlr]:
	# One or more consecutive whitespace characters:
	return (r'\s+', _trxnHdlr('delim', state._replace(name='default')))

def _keywordTrxnDef(state: LexState) -> tuple[str, _TrxnHdlr]:
	# Note the ``?ia:`` option for case-insensitive matching:
	return (f'(?ia:{reEsc(keywordAnd)}|{reEsc(keywordOr)})(?=\\s|\\Z)',
		_trxnHdlr('keyword', state._replace(name='default')))
	
def _metaPrefixTrxnDef(state: LexState, prefixes: Iterable[str]
	) -> tuple[str, _TrxnHdlr]:
	# Note the ``?ia:`` option for case-insensitive matching:
	return (f'''(?ia:{'|'.join(
		f'{reEsc(p)}{reEsc(metaPrefixSep)}' for p in prefixes)})''',
		_trxnHdlr('meta', state._replace(name='metaprefix')))

def _multipleGroupCloseTrxnDef(state: LexState) -> tuple[str, _TrxnHdlr]:
	# Closing parentheses before less than ``nOpen`` closing parentheses,
	# before a delim:
	n = state.nOpen - 1
	return (
		f'{reEsc(groupClose)}(?={reEsc(groupClose)}{{0,{n}}}(\\s|\\Z))',
		_trxnHdlr('close', LexState('default', n)))

def _wildcardTrxnDef(state: LexState) -> tuple[str, _TrxnHdlr]:
	# Wildcards before zero or more non-space ascii characters, before delim:
	return (
		f'{reEsc(wildcardChr)}+(?=[^\\s{_reNonAscii}]*(\\s|\\Z))',
		_trxnHdlr('wildcard', state._replace(name='literal')))

def _abbrevTrxnDef(state: LexState) -> tuple[str, _TrxnHdlr]:
	# Abbrev operator before zero or more non-space ascii characters,
	# before delim:
	return (
		f'{reEsc(abbrevChr)}(?=[^\\s{_reNonAscii}]*(\\s|\\Z))',
		_trxnHdlr('abbrev', state._replace(name='abbrev')))

def _nonClosingLiteralTrxnDef(state: LexState) -> tuple[str, _TrxnHdlr]:
	# Literal not ending in a closing parenthesis, before a delim:

	w = reEsc(wildcardChr)
	return (
		f'[^\\s{w}]*[^\\s{w}{reEsc(groupClose)}](?=\\s|\\Z)',
		_trxnHdlr('literal', state._replace(name='literal', nLiteralOpen=-1)))

def _literalBeforeWildcardTrxnDef(state: LexState) -> tuple[str, _TrxnHdlr]:
	# Ascii literal before a wildcard before zero or more non-space ascii
	# characters, before delim:

	w = reEsc(wildcardChr)
	return (
		f'[^\\s{w}{_reNonAscii}]+(?={w}[^\\s{_reNonAscii}]*(\\s|\\Z))',
		lambda m: (
			Token('literal', m),
			state._replace(
				name='literal',
				nLiteralOpen=_countOpenOrFindStray(m, state.nLiteralOpen))))

def _closingLiteralTrxnDef(state: LexState) -> tuple[str, _TrxnHdlr]:
	return (
		f'[^\\s{reEsc(wildcardChr)}]+{reEsc(groupClose)}(?=\\s|\\Z)',
		partial(_handleClosingLiteral, state))

def _handleClosingLiteral(state: LexState, lit: str) -> tuple[Token, LexState]:
	'''
	Without support for recursive regular expressions it's impossible to
	determine where a literal ends in all cases using regular expressions alone,
	because the number of trailing parentheses belonging to the literal depends
	on how many unclosed parentheses remain from earlier in the literal.

	This function will in examine *lit* with respect to the current lexer state
	and return a literal token of length 1 to ``len(len)`` inclusive, leaving
	behind any trailing parentheses which aren't part of the literal and
	should be used to close groups instead.
	'''
	assert state.nOpen > 0

	n = _countOpenOrFindStray(lit, state.nLiteralOpen)
	if n > 0:
		# There aren't enough trailing parentheses to balance the term.
		# Leave behind as many parentheses as possible up to ``nOpen``:
		m = _literalBeforeCloseGroupsPattern(state.nOpen).match(lit)
		assert m
		rvLit = m[0]
		
	elif n < 0:
		# ``n`` is the negative offset of the first stray.
		m = _literalBeforeCloseGroupsPattern(state.nOpen).match(lit[n:])
		assert m
		rvLit = lit[:n] + m[0]

	else:
		rvLit = lit

	return (Token('literal', rvLit),
		state._replace(name='literal', nLiteralOpen=-1))

@functools.cache
def _literalBeforeCloseGroupsPattern(nOpen: int) -> Pattern[str]:
	'''
	Matches as few characters as possible followed by
	up to ``nOpen`` closing parentheses.
	'''
	assert nOpen > 0
	return re.compile(f'\\A.*?(?={reEsc(groupClose)}{{1,{nOpen}}}\\Z)')

def _literalFallbackTrxnDef(state: LexState) -> tuple[str, _TrxnHdlr]:
	# Literal before delim:
	return (f'[^\\s]+(?=\\s|\\Z)',
		_trxnHdlr('literal', state._replace(name='literal', nLiteralOpen=-1)))

def _quoteOpenTrxnDef(state: LexState) -> tuple[str, _TrxnHdlr]:
	return (
		f'[{reEsc(quoteSingle)}{reEsc(quoteDouble)}]',
		lambda m: (
			Token('quote', m),
			state._replace(name='metavalue', quote=m)))

def _quoteCloseTrxnDef(state: LexState) -> tuple[str, _TrxnHdlr]:
	return (reEsc(state.quote),
		_trxnHdlr('quote', state._replace(name='default')))

#endregion

_groupOpenOrClosePattern = re.compile(
	f'[{reEsc(groupOpen)}{reEsc(groupClose)}]')

def _countOpenOrFindStray(s: str, nOpen: int = 0) -> int:
	'''
	Return the number of open parentheses in *s* which don't have a matching
	closing parenthesis, or the negative index of the first stray closing
	parenthesis.

	:param nOpen:
		The number of parentheses assumed to be already open before the
		beginning of the string.

	Examples:

	>>> _countOpenOrFindStray('a()b')
	0
	>>> _countOpenOrFindStray('a(b')
	1
	>>> _countOpenOrFindStray('(()(')
	2
	>>> _countOpenOrFindStray('a)bc')
	-3
	>>> _countOpenOrFindStray('a)b', 1)
	0
	'''
	assert nOpen >= 0

	for m in _groupOpenOrClosePattern.finditer(s):
		nOpen += (1 if m[0] == groupOpen else -1)
		if nOpen < 0:
			# stray closing parenthesis
			return m.start() - len(s)

	return nOpen

#region token transducers

def simplifyMetaTokens(toks: Iterable[Token]) -> Iterable[Token]:
	'''
	Concatenates meta-prefixes with meta-values, discarding any quotes or
	escapes.

	Examples:

	>>> [t.value for t in simplifyMetaTokens(
	...   tagExprTokens('source:x\\\\ yz', mode='edit'))]
	['source:x yz']

	>>> [t.value for t in simplifyMetaTokens(
	...   tagExprTokens('source:"x\\\\"yz"', mode='query'))]
	['source:x"yz']
	'''

	tokIter = iter(toks)

	nextTok: Optional[Token] = None

	def adv():
		nonlocal nextTok
		if nextTok:
			rv = nextTok
			nextTok = None
			return rv
		return next(tokIter, None)

	def peek():
		nonlocal nextTok
		if not nextTok:
			nextTok = next(tokIter, None)
		return nextTok

	while (t := adv()):
		if t.type == 'meta':
			# start of meta
			val = t.value
			i = t.start
			j = t.end

			q: Optional[bool] = None # quoted, unquoted or unknown
			
			while (t := peek()):
				if t.type == 'quote':
					j = t.end
					adv()
					if q:
						# end of quoted value
						t = None
						break
					elif q is None:
						# start of quoted value
						q = True
						continue
					else:
						# stray quote
						continue
				elif q is None:
					# unquoted value
					q = False

				if t.type == 'escape':
					j = t.end
					adv()
					continue
				elif t.type == 'literal':
					j = t.end
					val += t.value
					adv()
				else:
					# end of meta-value
					break

			yield Token('meta', value=val, start=i, end=j)

		else:
			yield t

def literaliseWildcardTokens(toks: Iterable[Token]) -> Iterable[Token]:
	'''
	Converts wildcards to literals and concatenates them with any adjacent
	literals.
	
	Example:

	>>> [t.value for t in literaliseWildcardTokens(
	...   tagExprTokens('ab*', mode='query'))]
	['ab*']
	'''

	tokIter = iter(toks)

	nextTok: Optional[Token] = None

	def adv():
		nonlocal nextTok
		if nextTok:
			rv = nextTok
			nextTok = None
			return rv
		return next(tokIter, None)

	def peek():
		nonlocal nextTok
		if not nextTok:
			nextTok = next(tokIter, None)
		return nextTok
	
	def nextType(ty: str) -> str:
		return 'wildcard' if ty == 'literal' else 'literal'
	
	while (t0 := adv()):
		if t0.type in ('wildcard', 'literal'):
			val = t0.value
			i = t0.start
			j = t0.end

			while (t1 := peek()):
				if t1.type == nextType(t0.type):
					j = t1.end
					val += t1.value
					t0 = adv()
				else:
					break

			yield Token('literal', value=val, start=i, end=j)
		else:
			yield t0

def literaliseAbbrevTokens(toks: Iterable[Token]) -> Iterable[Token]:
	'''
	Converts abbreviations to literals and concatenates them with any following
	literals.

	Example:

	>>> [t.value for t in literaliseAbbrevTokens(
	...   tagExprTokens('/a*b', mode='query'))]
	['/a, '*', 'b']
	'''

	tokIter = iter(toks)

	while (t0 := next(tokIter, None)):
		if t0.type == 'abbrev':
			t1 = next(tokIter, None)
			if t1 and t1.type == 'literal':
				yield Token('literal',
					value=(t0.value + t1.value),
					start=t0.start,
					end=t1.end)
			else:
				yield t0._replace(type='literal')
				if t1:
					yield t1
		else:
			yield t0

def operateNonAsciiTagTokens(
	fn: Callable[[Token], Optional[Token]],
	toks: Iterable[Token]) -> Iterable[Token]:
	'''
	Invokes *fn* for every tag literal containing non-ascii characters,
	allowing the token to be replaced or discarded in the output.
	*fn* must return a :class:`Token <Token>` or ``None``.

	Example:

	>>> [t.value for t in operateNonAsciiTagTokens(
	...   lambda t: t._replace(value=('x' + t.value)),
	...   tagExprTokens('skirt A\\x80 source:\\x80', mode='query'))]
	['skirt', ' ', 'xA\\x80', ' ', 'source:', '\\x80']
	'''

	state: Literal[None, 'meta', 'quote'] = None

	t: Optional[Token]
	for t in toks:
		assert t
		if t.type == 'meta':
			# preceeding a meta-value
			state = 'meta'

		elif t.type == 'quote':
			if state == 'meta':
				# opening quote
				state = 'quote'
			else:
				# closing quote
				state = None

		elif t.type not in ('literal', 'escape'):
			# end of meta-value
			state = None

		elif t.type == 'literal' and state is None and not t.value.isascii():
			# non-ascii tag
			t = fn(t)

		if t is not None:
			yield t

def casefoldTokens(toks: Iterable[Token]) -> Iterable[Token]:
	'''
	Casefolds the values of meta-prefixes, keywords and ASCII non-meta literals.
	Meta-values and non-ASCII literals remain unaltered.

	Examples:

	>>> [t.value for t in casefoldTokens(
	...   tagExprTokens('RE:ZERO (/AE OR HAT) IS:SFW', mode='query'))]
	['re:zero', ' ', '(', '/', 'ae', ' ', 'or', ' ', 'hat', ')', ' ',
	  'is:', 'SFW']

	>>> [t.value for t in casefoldTokens(
	...   tagExprTokens('SouRcE:"X\\\\"Yz")', mode='query'))]
	['source:', '"', 'X', '\\\\', '"Yz', '"', ')']

	>>> [t.value for t in casefoldTokens(
	...   tagExprTokens('A\\x80', mode='query'))]
	['A\\x80']
	'''

	state: Literal[None, 'meta', 'quote'] = None

	def fold(tok):
		return tok._replace(value=tok.value.casefold())
	
	for t in toks:
		if t.type == 'keyword':
			t = fold(t)

		elif t.type == 'meta':
			# preceeding a meta-value
			state = 'meta'

			# casefoldable meta-prefix
			t = fold(t)

		elif t.type == 'quote':
			if state == 'meta':
				# opening quote
				state = 'quote'
			else:
				# closing quote
				state = None

		elif t.type not in ('literal', 'escape'):
			# end of meta-value
			state = None

		elif t.type == 'literal' and state is None and t.value.isascii():
			# casefoldable literal
			t = fold(t)

		yield t

#endregion
