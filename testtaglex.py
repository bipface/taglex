import re
from pprint import pformat
from unittest import TestCase
from collections.abc import Iterable, Collection, Sequence
from typing import Optional, Any
from functools import partial, partialmethod

import taglex
from taglex import Token

class TagLexTest(TestCase):
	def testTagExprTokensOffsets(self):
		toks = tuple(taglex.tagExprTokens(
			're:zero (/ae or hat) is:sfw', mode='query'))

		self.assertSequenceEqual(
			toks,
			(
				Token('literal', value='re:zero', start=0, end=7),
				Token('delim', value=' ', start=7, end=8),
				Token('open', value='(', start=8, end=9),
				Token('abbrev', value='/', start=9, end=10),
				Token('literal', value='ae', start=10, end=12),
				Token('delim', value=' ', start=12, end=13),
				Token('keyword', value='or', start=13, end=15),
				Token('delim', value=' ', start=15, end=16),
				Token('literal', value='hat', start=16, end=19),
				Token('close', value=')', start=19, end=20),
				Token('delim', value=' ', start=20, end=21),
				Token('meta', value='is:', start=21, end=24),
				Token('literal', value='sfw', start=24, end=27),
			))

	def testTagExprTokensEditModeWithWhitespace(self):
		self.assertSequenceEqual(
			_lexEdit(f'a{_allSpaceChrs}b'),
			('a', Token('delim', _allSpaceChrs), 'b'),
			msg='Contiguous whitespace should be collapsed.')

	def testTagExprTokensQueryModeWithWhitespace(self):
		self.assertSequenceEqual(
			_lexQuery(f'a{_allSpaceChrs}b'),
			('a', Token('delim', _allSpaceChrs), 'b'),
			msg='Contiguous whitespace should be collapsed.')

	def testTagExprTokensEditModeWithWhitespaceInMetaValue(self):
		self.assertSequenceEqual(
			_lexEdit(f'source:"{_allSpaceChrs}"'),
			(
				Token('meta', 'source:'),
				Token('quote', taglex.quoteDouble),
				_allSpaceChrs,
				Token('quote', taglex.quoteDouble),
			),
			msg='Contiguous whitespace inside meta-values should be preserved.')

	def testTagExprTokensQueryModeWithWhitespaceInMetaValue(self):
		self.assertSequenceEqual(
			_lexQuery(f'source:"{_allSpaceChrs}"'),
			(
				Token('meta', 'source:'),
				Token('quote', taglex.quoteDouble),
				_allSpaceChrs,
				Token('quote', taglex.quoteDouble),
			),
			msg='Contiguous whitespace inside meta-values should be preserved.')

	def testTagExprTokensQueryModeWithContiguousWildcards(self):
		self.assertSequenceEqual(
			_lexQuery(f'a**b'),
			(
				'a',
				Token('wildcard', '**'),
				'b',
			),
			msg='Contiguous wildcards should be collapsed.')

	#region casefold transducer

	def testCasefold(self):
		self.assertSequenceEqual(
			_lexQuery('RE:ZERO (/AE OR HAT) IS:SFW',
				(taglex.casefoldTokens,)),
			(
				're:zero',
				Token('delim', ' '),
				Token('open', '('),
				Token('abbrev', '/'),
				'ae',
				Token('delim', ' '),
				Token('keyword', 'or'),
				Token('delim', ' '),
				'hat',
				Token('close', ')'),
				Token('delim', ' '),
				Token('meta', 'is:'),
				'SFW',
			))

	def testCasefoldWithNonAscii(self):
		self.assertSequenceEqual(
			_lexQuery('A\x80',
				(taglex.casefoldTokens,)),
			('A\x80',))

	def testCasefoldWithEscapes(self):
		self.assertSequenceEqual(
			_lexEdit('SouRcE:X\\ Yz ZXCV',
				(taglex.casefoldTokens,)),
			(
				Token('meta', 'source:'),
				'X',
				Token('escape', '\\'),
				' Yz',
				Token('delim', ' '),
				'zxcv',
			))

	def testCasefoldWithQuotesAndEscapes(self):
		self.assertSequenceEqual(
			_lexQuery('SouRcE:"X\\"Yz"ZXCV',
				(taglex.casefoldTokens,)),
			(
				Token('meta', 'source:'),
				Token('quote', '"'),
				'X',
				Token('escape', '\\'),
				'"Yz',
				Token('quote', '"'),
				'zxcv',
			))

	#endregion

	#region simplifyMetaTokens transducer

	def testSimplifyMetaTokensWithQuotesAndEscapes(self):
		toks = taglex.simplifyMetaTokens(
			taglex.tagExprTokens(
				'SouRcE:"X\\"Yz"ZXCV', mode='query'))

		self.assertSequenceEqual(
			tuple(toks),
			(
				Token('meta', 'SouRcE:X"Yz', start=0, end=14),
				Token('literal', 'ZXCV', start=14, end=18),
			))

	def testSimplifyMetaTokensEmpty(self):
		toks = taglex.simplifyMetaTokens(
			taglex.tagExprTokens(
				'source:', mode='query'))

		self.assertSequenceEqual(
			tuple(toks),
			(Token('meta', 'source:', start=0, end=7),))

	def testSimplifyMetaTokensWithUnclosedQuoteEmpty(self):
		toks = taglex.simplifyMetaTokens(
			taglex.tagExprTokens(
				'source:"', mode='query'))

		self.assertSequenceEqual(
			tuple(toks),
			(Token('meta', 'source:', start=0, end=8),))

	def testSimplifyMetaTokensWithUnclosedQuote(self):
		toks = taglex.simplifyMetaTokens(
			taglex.tagExprTokens(
				'source:"a', mode='query'))

		self.assertSequenceEqual(
			tuple(toks),
			(Token('meta', 'source:a', start=0, end=9),))

	def testSimplifyMetaTokensWithEscapedSpace(self):
		toks = taglex.simplifyMetaTokens(
			taglex.tagExprTokens(
				'source:\\ ab c', mode='edit'))

		self.assertSequenceEqual(
			tuple(toks),
			(
				Token('meta', 'source: ab', start=0, end=11),
				Token('delim', ' ', start=11, end=12),
				Token('literal', 'c', start=12, end=13),
			))

	def testSimplifyMetaTokensContiguous(self):
		toks = taglex.simplifyMetaTokens(
			taglex.tagExprTokens(
				'source:"a"source:"b"', mode='query'))

		self.assertSequenceEqual(
			tuple(toks),
			(
				Token('meta', 'source:a', start=0, end=10),
				Token('meta', 'source:b', start=10, end=20),
			))
			
	#endregion

	#region operateNonAsciiTagTokens transducer

	def testOperateNonAsciiTagTokensAscii(self):
		def err(t):
			raise Exception()

		self.assertSequenceEqual(
			_lexQuery('a',
				(partial(taglex.operateNonAsciiTagTokens, err),)),
			('a',))

	def testOperateNonAsciiTagTokensAsciiWithOperators(self):
		def err(t):
			raise Exception()

		self.assertSequenceEqual(
			_lexQuery('-/*a',
				(partial(taglex.operateNonAsciiTagTokens, err),)),
			(
				Token('unary', '-'),
				Token('abbrev', '/'),
				Token('wildcard', '*'),
				'a',
			))

	def testOperateNonAsciiTagTokensMeta(self):
		def err(t):
			raise Exception()

		self.assertSequenceEqual(
			_lexQuery('source:A\x80',
				(partial(taglex.operateNonAsciiTagTokens, err),)),
			(
				Token('meta', 'source:'),
				'A\x80',
			))

	def testOperateNonAsciiTagTokensPassthrough(self):
		self.assertSequenceEqual(
			_lexQuery('A\x80',
				(partial(taglex.operateNonAsciiTagTokens, lambda t: t),)),
			('A\x80',))

	def testOperateNonAsciiTagTokensDiscard(self):
		self.assertSequenceEqual(
			_lexQuery('A\x80',
				(partial(taglex.operateNonAsciiTagTokens, lambda t: None),)),
			())

	def testOperateNonAsciiTagTokensReplace(self):
		self.assertSequenceEqual(
			_lexQuery('A\x80',
				(partial(taglex.operateNonAsciiTagTokens,
					lambda t: t._replace(type='meta', value='\x80:')),)),
			(Token('meta', '\x80:'),))

	def testOperateNonAsciiTagTokensWithOperators(self):
		self.assertSequenceEqual(
			_lexQuery('-/*a\x80',
				(partial(taglex.operateNonAsciiTagTokens, lambda t: t),)),
			(
				Token('unary', '-'),
				'/*a\x80',
			))
			
	#endregion

	#region literaliseAbbrevTokens transducer
	
	def testLiteraliseAbbrevTokens(self):
		toks = tuple(
			taglex.literaliseAbbrevTokens(
				taglex.tagExprTokens('/ab', mode='query')))

		self.assertSequenceEqual(
			toks,
			(Token('literal', '/ab', start=0, end=3),))
	
	def testLiteraliseAbbrevTokensEmptyOnly(self):
		self.assertSequenceEqual(
			_lexQuery('/', (taglex.literaliseAbbrevTokens,)),
			('/',))
	
	def testLiteraliseAbbrevTokensEmpty(self):
		self.assertSequenceEqual(
			_lexQuery('/ a', (taglex.literaliseAbbrevTokens,)),
			('/', Token('delim', ' '), 'a'))
	
	def testLiteraliseAbbrevTokensWithWildcardOnly(self):
		self.assertSequenceEqual(
			_lexQuery('/*', (taglex.literaliseAbbrevTokens,)),
			('/', Token('wildcard', '*'),))

	def testLiteraliseAbbrevTokensWithWildcard(self):
		self.assertSequenceEqual(
			_lexQuery('/a*', (taglex.literaliseAbbrevTokens,)),
			('/a', Token('wildcard', '*'),))

	#endregion

	#region literaliseWildcardTokens transducer
	
	def testLiteraliseWildcardTokensWithWildcardOnly(self):
		self.assertSequenceEqual(
			_lexQuery('**', (taglex.literaliseWildcardTokens,)),
			('**',))
	
	def testLiteraliseWildcardTokensWithWildcardStart(self):
		self.assertSequenceEqual(
			_lexQuery('*a', (taglex.literaliseWildcardTokens,)),
			('*a',))
	
	def testLiteraliseWildcardTokensWithWildcardEnd(self):
		toks = tuple(
			taglex.literaliseWildcardTokens(
				taglex.tagExprTokens('a*', mode='query')))

		self.assertSequenceEqual(
			toks,
			(Token('literal', 'a*', start=0, end=2),))
	
	def testLiteraliseWildcardTokensWithWildcardInfix(self):
		self.assertSequenceEqual(
			_lexQuery('a*b', (taglex.literaliseWildcardTokens,)),
			('a*b',))
	
	def testLiteraliseWildcardTokensWithWildcardMultiple(self):
		self.assertSequenceEqual(
			_lexQuery('a*b*c', (taglex.literaliseWildcardTokens,)),
			('a*b*c',))
	
	def testLiteraliseWildcardTokensWithWildcardMultipleStartAndEnd(self):
		self.assertSequenceEqual(
			_lexQuery('*a*b*c*', (taglex.literaliseWildcardTokens,)),
			('*a*b*c*',))

	def testLiteraliseWildcardTokensWithWildcardMeta(self):
		self.assertSequenceEqual(
			_lexQuery('source:*', (taglex.literaliseWildcardTokens,)),
			(Token('meta', 'source:'), '*',))
			
	def testLiteraliseWildcardTokensWithWildcardOnlyAbbrev(self):
		self.assertSequenceEqual(
			_lexQuery('/*', (taglex.literaliseWildcardTokens,)),
			(Token('abbrev', '/'), '*',))
			
	def testLiteraliseWildcardTokensWithWildcardAbbrev(self):
		self.assertSequenceEqual(
			_lexQuery('/*a', (taglex.literaliseWildcardTokens,)),
			(Token('abbrev', '/'), '*a',))
			
	#endregion

def _lexQuery(s: str, txducers = ()):
	return _lex(s, 'query', txducers)

def _lexEdit(s: str, txducers = ()):
	return _lex(s, 'edit', txducers)

def _lex(s: str, mode: taglex.LexMode, txducers = ()):
	toks = taglex.tagExprTokens(s, mode=mode)
	
	for fn in txducers:
		toks = fn(toks)
	
	return tuple(map(_simplifyTok, toks))

def _exprsAndTokensForEditModeTests() -> Sequence[tuple[str, Sequence[Any]]]:
	'''
	See https://github.com/danbooru/danbooru/blob/master/test/unit/post_test.rb
	'''

	o = Token('open', taglex.groupOpen)
	c = Token('close', taglex.groupClose)
	d = Token('delim', ' ')
	esc = Token('escape', taglex.escapeChr)
	m = partial(Token, 'meta')
	squote = Token('quote', taglex.quoteSingle)
	dquote = Token('quote', taglex.quoteDouble)
	uNot = Token('unary', taglex.unaryNot)
	abbrev = Token('abbrev', taglex.abbrevChr)

	return (
		# examples from ``post_test.rb``:

		('foo bar', ('foo', d, 'bar')),
		('aaa /hr', ('aaa', d, abbrev, 'hr')),
		('asd char:a_bad_tag', ('asd', d, m('char:'), 'a_bad_tag')),
		('asd a_bad_tag -a_bad_tag', ('asd', d, 'a_bad_tag', d, uNot, 'a_bad_tag')),
		('char:hoge', (m('char:'), 'hoge')),
		('tagme char:copy:blah', ('tagme', d, m('char:'), 'copy:blah')),
		('char:someone_(cosplay) test_school_uniform', (m('char:'), 'someone_(cosplay)', d, 'test_school_uniform')),
		('aaa pool:abc', ('aaa', d, m('pool:'), 'abc')),
		('aaa -pool:abc', ('aaa', d, uNot, m('pool:'), 'abc')),
		('aaa newpool:ichigo_100%', ('aaa', d, m('newpool:'), 'ichigo_100%')),
		('aaa newpool:"foo bar baz" bbb', ('aaa', d, m('newpool:'), dquote, 'foo bar baz', dquote, d, 'bbb')),
		('aaa newpool:\'foo bar baz\' bbb', ('aaa', d, m('newpool:'), squote, 'foo bar baz', squote, d, 'bbb')),
		('aaa newpool:foo\\ bar\\ baz bbb', ('aaa', d, m('newpool:'), 'foo', esc, ' bar', esc, ' baz', d, 'bbb')),
		('aaa fav:self fav:me', ('aaa', d, m('fav:'), 'self', d, m('fav:'), 'me')),
		('aaa -fav:self -fav:me', ('aaa', d, uNot, m('fav:'), 'self', d, uNot, m('fav:'), 'me')),
		('aaa child:1..2', ('aaa', d, m('child:'), '1..2')),
		('aaa child:none', ('aaa', d, m('child:'), 'none')),
		('aaa child:', ('aaa', d, m('child:'))),
		('aaa source:"foo bar baz" bbb', ('aaa', d, m('source:'), dquote, 'foo bar baz', dquote, d, 'bbb')),
		('aaa source:\'foo bar baz\' bbb', ('aaa', d, m('source:'), squote, 'foo bar baz', squote, d, 'bbb')),
		('aaa bbb ccc -bbb', ('aaa', d, 'bbb', d, 'ccc', d, uNot, 'bbb')),
		('aaa hair_ribbon hakurei_reimu -/hr', ('aaa', d, 'hair_ribbon', d, 'hakurei_reimu', d, uNot, abbrev, 'hr')),
		('aaa source:https://www.example.com', ('aaa', d, m('source:'), 'https://www.example.com')),

		# tags:

		('(', ('(',)),
		(')', (')',)),
		('()', ('()',)),
		('(a)', ('(a)',)),
		('( a )', ('(', d, 'a', d, ')')),
		('*', ('*',)),
		('~', ('~',)),
		('a and b', ('a', d, 'and', d, 'b')),
		('a or b', ('a', d, 'or', d, 'b')),
		('AaA', ('AaA',)),

		# tags with unary ops:

		('-a', (uNot, 'a')),
		('~a', ('~a',)),
		('~-a', ('~-a',)),
		('--a', (uNot, '-a',)),

		# meta-prefixes:

		('source:x', (m('source:'), 'x')),
		('source:*', (m('source:'), '*')),
		('source:""', (m('source:'), dquote, dquote)),
		('source:"x', (m('source:'), dquote, 'x')),
		('source:"x\\"', (m('source:'), dquote, 'x', esc, '"')),
		('source:\'x', (m('source:'), squote, 'x')),
		('source:"\\x"', (m('source:'), dquote, '\\x', dquote)),
		('source:"\\"x"', (m('source:'), dquote, esc, '"x', dquote)),
		('source:"\\"x\\""', (m('source:'), dquote, esc, '"x', esc, '"', dquote)),
		('source:"\\\\"', (m('source:'), dquote, '\\', esc, '"')),
		('source:"\\\\x"', (m('source:'), dquote, '\\\\x', dquote)),
		('source:a"b', (m('source:'), 'a"b')),
		('source:a\\b', (m('source:'), 'a\\b')),
		('source:a\\"b', (m('source:'), 'a\\"b')),
		('source:a\\\\b', (m('source:'), 'a\\\\b')),
		('sOuRcE:x', (m('sOuRcE:'), 'x')),
		('source:AaA', (m('source:'), 'AaA')),

		('source:a\\"b\\\'c\\ d e', (m('source:'), 'a\\"b\\\'c', esc, ' d', d, 'e')),
		('source:"a\\"b\\\'c\\ d" e', (m('source:'), dquote, 'a', esc, '"b\\\'c\\ d', dquote, d, 'e')),
		('source:"a\\\\"b\\\'c\\ d" e', (m('source:'), dquote, 'a\\', esc, '"b\\\'c\\ d', dquote, d, 'e')),
		('source:\'a\\"b\\\\\'c\\ d\' e', (m('source:'), squote, 'a\\"b\\', esc, '\'c\\ d', squote, d, 'e')),
		('source:\\', (m('source:'), '\\')),
		('source:\\ a', (m('source:'), esc, ' a')),
		('source:\\\\ a', (m('source:'), '\\', esc, ' a')),
		('source:\'\\\' a', (m('source:'), squote, esc, '\' a')),
		('source:\'\\\\\' a', (m('source:'), squote, '\\', esc, '\' a')),

		('order:a', ('order:a',)),

		# https://github.com/danbooru/danbooru/issues/5292
		('source:a\\  b', (m('source:'), 'a', esc, ' ', d, 'b')),
		('b source:a\\ ', ('b', d, m('source:'), 'a', esc, ' ')),
		('b source:a\\  ', ('b', d, m('source:'), 'a', esc, ' ', d)),

		# meta-prefixes with unary ops:
		
		('-source:x', (uNot, m('source:'), 'x')),
		('~source:x', ('~source:x',)),
		('~-source:x', ('~-source:x',)),
		('--source:x', (uNot, '-source:x',)),
		('-order:a', (uNot, 'order:a',)),

		# abbreviations:

		('/', (abbrev,)),
		('/a', (abbrev, 'a')),
		('//', (abbrev, '/')),
		('//a', (abbrev, '/a')),
		('-/a', (uNot, abbrev, 'a')),
		('/-a', (abbrev, '-a')),
		('/*', (abbrev, '*')),
		('/)', (abbrev, ')')),
		('/(', (abbrev, '(')),
		('(/)', ('(/)',)),
		('/source:', (abbrev, 'source:')),
		('/source:a', (abbrev, 'source:a')),

		# non-ascii:

		('\u00e6', ('\u00e6',)),
		('\u00e6*', ('\u00e6*',)),
		('*\u00e6', ('*\u00e6',)),
		('a\u00e6', ('a\u00e6',)),
		('a*\u00e6', ('a*\u00e6',)),
		('-/a\u00e6', (uNot, '/a\u00e6',)),
		('-\u00e6', (uNot, '\u00e6',)),
		('/a\u00e6', ('/a\u00e6',)),
		('/a\u00e6*', ('/a\u00e6*',)),
		('/a*\u00e6', ('/a*\u00e6',)),
		('/*a\u00e6', ('/*a\u00e6',)),
		('-/a\u00e6', (uNot, '/a\u00e6',)),
		('\u0080*', ('\u0080*',)),
		('\U0010ffff*', ('\U0010ffff*',)),
		('\u017fource:', ('\u017fource:',)),
	)

def _exprsAndTokensForQueryModeTests() -> Sequence[tuple[str, Sequence[Any]]]:
	'''
	See https://github.com/danbooru/danbooru/blob/master/test/unit/post_query_parser_test.rb
	See https://github.com/danbooru/danbooru/blob/master/test/unit/tag_test.rb
	'''

	o = Token('open', taglex.groupOpen)
	c = Token('close', taglex.groupClose)
	wild = Token('wildcard', taglex.wildcardChr)
	d = Token('delim', ' ')
	m = partial(Token, 'meta')
	squote = Token('quote', taglex.quoteSingle)
	dquote = Token('quote', taglex.quoteDouble)
	kwOr = Token('keyword', taglex.keywordOr)
	kwAnd = Token('keyword', taglex.keywordAnd)
	uOr = Token('unary', taglex.unaryOr)
	uNot = Token('unary', taglex.unaryNot)
	esc = Token('escape', taglex.escapeChr)
	abbrev = Token('abbrev', taglex.abbrevChr)

	return (
		# empty:

		('', ()),
		(' ', (d,)),

		# parentheses:

		('a))', ('a))',)),
		(';)', (';)',)),
		('(9)', (o, '9', c)),

		("foo_(bar)", ('foo_(bar)',)),
		("(foo_(bar))", (o, "foo_(bar)", c)),
		("((foo_(bar)))", (o, o, "foo_(bar)", c, c)),

		("foo_(bar_(baz))", ("foo_(bar_(baz))",)),
		("(foo_(bar_(baz)))", (o, "foo_(bar_(baz))", c)),
		("(foo_(bar_baz)))", (o, "foo_(bar_baz))", c)),

		("abc_(def) ghi", ("abc_(def)", d, "ghi")),
		("(abc_(def) ghi)", (o, "abc_(def)", d, "ghi", c)),
		("((abc_(def)) ghi)", (o, o, "abc_(def)", c, d, 'ghi', c)),

		("abc def_(ghi)", ("abc", d, "def_(ghi)")),
		("(abc def_(ghi))", (o, "abc", d, "def_(ghi)", c)),
		("(abc (def_(ghi)))", (o, "abc", d, o, "def_(ghi)", c, c)),

		("abc_(def) ghi_(jkl)", ("abc_(def)", d, "ghi_(jkl)")),
		("(abc_(def) ghi_(jkl))", (o, "abc_(def)", d, "ghi_(jkl)", c)),

		(":)", (":)",)),
		("(:))", (o, ":)", c)),
		("(:)", (o, ":)")),

		("(:) >:))", (o, ":)", d, ">:)", c)),
		("(:) >:)", (o, ":)", d, ">:)")),

		("*)", (wild, ')')),
		("(*)", (o, wild, c)),

		("(foo*)", (o, "foo", wild, c)),
		("foo*)", ("foo", wild, ")")),

		("foo*) bar", ("foo", wild, ")", d, "bar")),
		("(foo*) bar", (o, "foo", wild, c, d, "bar")),
		("(foo*) bar)", (o, 'foo', wild, c, d, 'bar)')),

		("*_(foo)", (wild, '_(foo)')),
		("(*_(foo))", (o, wild, '_(foo)', c)),

		("(*_(foo) bar)", (o, wild, '_(foo)', d, 'bar', c)),
		("((*_(foo)) bar)", (o, o, wild, '_(foo)', c, d, 'bar', c)),
		("(bar *_(foo))", (o, 'bar', d, wild, '_(foo)', c)),
		("(bar (*_(foo)))", (o, 'bar', d, o, wild, '_(foo)', c, c)),

		("(note:a)", (o, m('note:'), 'a', c)),
		("(note:(a)", (o, m('note:'), '(a', c)),
		("(note:(a))", (o, m('note:'), '(a)', c)),

		("(note:a note:b)", (o, m('note:'), 'a', d, m('note:'), 'b', c)),
		("(note:a) note:b)", (o, m('note:'), 'a', c, d, m('note:'), 'b)')),

		('(note:"a)" note:b)', (o, m('note:'), dquote, 'a)', dquote, d, m('note:'), 'b', c)),

		# basic queries:

		("a b", ('a', d, 'b')),
		("a or b", ('a', d, kwOr, d, 'b')),
		("~a ~b", (uOr, 'a', d, uOr, 'b')),

		("-a", (uNot, 'a')),
		("a -b", ('a', d, uNot, 'b')),

		("-fav:a", (uNot, m('fav:'), 'a')),

		("fav:a fav:b", (m('fav:'), 'a', d, m('fav:'), 'b')),


		# meta prefixes:

		("fav:a", (m('fav:'), 'a')),
		("user:a", (m('user:'), 'a')),
		("pool:a", (m('pool:'), 'a')),
		("order:a", (m('order:'), 'a')),
		("source:a", (m('source:'), 'a')),

		("FAV:a", (m('FAV:'), 'a')),
		("fav:A", (m('fav:'), 'A')),

		("~fav:a", (uOr, m('fav:'), 'a')),
		("-fav:a", (uNot, m('fav:'), 'a')),

		("fav:a fav:b", (m('fav:'), 'a', d, m('fav:'), 'b')),
		("~fav:a ~fav:b", (uOr, m('fav:'), 'a', d, uOr, m('fav:'), 'b')),
		("fav:a or fav:b", (m('fav:'), 'a', d, kwOr, d, m('fav:'), 'b')),

		("(fav:a)", (o, m('fav:'), 'a', c)),
		("fav:(a)", (m('fav:'), '(a)')),
		("(fav:(a)", (o, m('fav:'), '(a', c)),

		('source:"foo bar"', (m('source:'), dquote, 'foo bar', dquote)),
		('source:foobar"(', (m('source:'), 'foobar"(')),
		('source:', (m('source:'),)),
		('source:""', (m('source:'), dquote, dquote)),
		('source:"\\""', (m('source:'), dquote, esc, '"', dquote)),
		('source:"don\'t say \\"lazy\\" okay"', (m('source:'), dquote, 'don\'t say ', esc, '"lazy', esc, '" okay', dquote)),
		('(a (source:\'foo)bar\'))', (o, 'a', d, o, m('source:'), squote, 'foo)bar', squote, c, c)),

		('''source:'foo bar\'''', (m('source:'), squote, 'foo bar', squote)),
		("source:foobar'(", (m('source:'), '''foobar'(''')),
		("source:''", (m('source:'), squote, squote)),
		('source:\'\\\'\'', (m('source:'), squote, esc, '\'', squote)),
		('source:\'don\\\'t say "lazy" okay\'', (m('source:'), squote, 'don', esc, '\'t say "lazy" okay', squote)),
		('(a (source:\'foo)bar\'))', (o, 'a', d, o, m('source:'), squote, 'foo)bar', squote, c, c)),

		# meta-prefix synonyms:

		("comments:0", (m('comments:'), '0')),
		("deleted_comments:0", (m('deleted_comments:'), '0')),
		("active_comments:0", (m('active_comments:'), '0')),
		("notes:0", (m('notes:'), '0')),
		("deleted_notes:0", (m('deleted_notes:'), '0')),
		("active_notes:0", (m('active_notes:'), '0')),
		("flags:0", (m('flags:'), '0')),
		("children:0", (m('children:'), '0')),
		("deleted_children:0", (m('deleted_children:'), '0')),
		("active_children:0", (m('active_children:'), '0')),
		("pools:0", (m('pools:'), '0')),
		("deleted_pools:0", (m('deleted_pools:'), '0')),
		("active_pools:0", (m('active_pools:'), '0')),
		("series_pools:0", (m('series_pools:'), '0')),
		("collection_pools:0", (m('collection_pools:'), '0')),
		("appeals:0", (m('appeals:'), '0')),
		("approvals:0", (m('approvals:'), '0')),
		("replacements:0", (m('replacements:'), '0')),

		("order:comments", (m('order:'), 'comments')),
		("order:notes", (m('order:'), 'notes')),
		("order:flags", (m('order:'), 'flags')),
		("order:children", (m('order:'), 'children')),
		("order:pools", (m('order:'), 'pools')),
		("order:appeals", (m('order:'), 'appeals')),
		("order:approvals", (m('order:'), 'approvals')),
		("order:replacements", (m('order:'), 'replacements')),

		# wildcards:

		("*", (wild,)),
		("*a", (wild, 'a')),
		("a*", ('a', wild)),
		("*a*", (wild, 'a', wild)),
		("a*b", ('a', wild, 'b')),

		("* b", (wild, d, 'b')),
		("*a b", (wild, 'a', d, 'b')),
		("a* b", ('a', wild, d, 'b')),
		("*a* b", (wild, 'a', wild, d, 'b')),

		("a *", ('a', d, wild)),
		("a *b", ('a', d, wild, 'b')),
		("a b*", ('a', d, 'b', wild)),
		("a *b*", ('a', d, wild, 'b', wild)),

		("a -*", ('a', d, uNot, wild)),
		("a -b*", ('a', d, uNot, 'b', wild)),
		("a -*b", ('a', d, uNot, wild, 'b')),
		("a -*b*", ('a', d, uNot, wild, 'b', wild)),

		("~a ~*", (uOr, 'a', d, uOr, wild)),
		("~* ~a", (uOr, wild, d, uOr, 'a')),
		("~a ~*a", (uOr, 'a', d, uOr, wild, 'a')),
		("~*a ~a", (uOr, wild, 'a', d, uOr, 'a')),

		("a or a*", ('a', d, kwOr, d, 'a', wild)),
		("a and a*", ('a', d, kwAnd, d, 'a', wild)),

		("a* b*", ('a', wild, d, 'b', wild)),
		("a* or b*", ('a', wild, d, kwOr, d, 'b', wild)),

		("a b* c", ('a', d, 'b', wild, d, 'c')),
		("a -* c", ('a', d, uNot, wild, d, 'c')),

		# single-tag queries:

		("a", ("a",)),
		("a ", ("a", d)),
		(" a", (d, "a")),
		(" a ", (d, "a", d)),
		("(a)", (o, "a", c)),
		("( a)", (o, d, "a", c)),
		("(a )", (o, "a", d, c)),
		(" ( a ) ", (d, o, d, "a", d, c, d)),
		("((a))", (o, o, "a", c, c)),
		("( ( a ) )", (o, d, o, d, "a", d, c, d, c)),
		(" ( ( a ) ) ", (d, o, d, o, d, "a", d, c, d, c, d)),

		# nested AND queries:

		("a b", ('a', d, 'b'),),
		("(a b)", (o, 'a', d, 'b', c)),
		("a (b)", ('a', d, o, 'b', c)),
		("(a) b", (o, 'a', c, d, 'b')),
		("(a) (b)", (o, 'a', c, d, o, 'b', c)),
		("((a) (b))", (o, o, 'a', c, d, o, 'b', c, c)),

		("a b c", ('a', d, 'b', d, 'c')),
		("(a b) c", (o, 'a', d, 'b', c, d, 'c')),
		("((a) b) c", (o, o, 'a', c, d, 'b', c, d, 'c')),
		("(((a) b) c)", (o, o, o, 'a', c, d, 'b', c, d, 'c', c)),
		("((a b) c)", (o, o, 'a', d, 'b', c, d, 'c', c)),
		("((a) (b) (c))", (o, o, 'a', c, d, o, 'b', c, d, o, 'c', c, c)),

		("a (b c)", ('a', d, o, 'b', d, 'c', c)),
		("a (b (c))", ('a', d, o, 'b', d, o, 'c', c, c)),
		("(a (b (c)))", (o, 'a', d, o, 'b', d, o, 'c', c, c, c)),
		("(a (b c))", (o, 'a', d, o, 'b', d, 'c', c, c)),
		("(a b c)", (o, 'a', d, 'b', d, 'c', c)),

		("a and b", ('a', d, kwAnd, d, 'b')),
		("a AND b", ('a', d, Token('keyword', taglex.keywordAnd.upper()), d, 'b')),
		("(a and b)", (o, 'a', d, kwAnd, d, 'b', c)),
		("a and b and c", ('a', d, kwAnd, d, 'b', d, kwAnd, d, 'c')),
		("(a and b) and c", (o, 'a', d, kwAnd, d, 'b', c, d, kwAnd, d, 'c')),
		("a and (b and c)", ('a', d, kwAnd, d, o, 'b', d, kwAnd, d, 'c', c)),
		("(a and b and c)", (o, 'a', d, kwAnd, d, 'b', d, kwAnd, d, 'c', c)),


		# nested OR queries:

		("a or b", ('a', d, kwOr, d, 'b')),
		("a OR b", ('a', d, Token('keyword', taglex.keywordOr.upper()), d, 'b')),
		("(a or b)", (o, 'a', d, kwOr, d, 'b', c)),
		("(a) or (b)", (o, 'a', c, d, kwOr, d, o, 'b', c)),

		("a or b or c", ('a', d, kwOr, d, 'b', d, kwOr, d, 'c')),
		("(a or b) or c", (o, 'a', d, kwOr, d, 'b', c, d, kwOr, d, 'c')),
		("a or (b or c)", ('a', d, kwOr, d, o, 'b', d, kwOr, d, 'c', c)),
		("(a or b or c)", (o, 'a', d, kwOr, d, 'b', d, kwOr, d, 'c', c)),

		("a or (b or (c or d))", ('a', d, kwOr, d, o, 'b', d, kwOr, d, o, 'c', d, kwOr, d, 'd', c, c)),
		("((a or b) or c) or d", (o, o, 'a', d, kwOr, d, 'b', c, d, kwOr, d, 'c', c, d, kwOr, d, 'd')),
		("(a or b) or (c or d)", (o, 'a', d, kwOr, d, 'b', c, d, kwOr, d, o, 'c', d, kwOr, d, 'd', c)),

		# ~ operator:

		("~a ~b", (uOr, 'a', d, uOr, 'b')),
		("~a ~b ~c", (uOr, 'a', d, uOr, 'b', d, uOr, 'c')),
		("~a ~b ~c ~d", (uOr, 'a', d, uOr, 'b', d, uOr, 'c', d, uOr, 'd')),

		("~a", (uOr, "a")),
		("(~a)", (o, uOr, "a", c)),
		("~(a)", (uOr, o, 'a', c)),
		("~(~a)", (uOr, o, uOr, 'a', c)),
		("~(~(~a))", (uOr, o, uOr, o, uOr, 'a', c, c)),

		("~(-a)", (uOr, o, uNot, 'a', c)),
		("-(~a)", (uNot, o, uOr, 'a', c)),
		("-(~(-(~a)))", (uNot, o, uOr, o, uNot, o, uOr, 'a', c, c, c)),
		("~(-(~(-a)))", (uOr, o, uNot, o, uOr, o, uNot, 'a', c, c, c)),

		("a ~b", ('a', d, uOr, 'b')),
		("~a b", (uOr, 'a', d, 'b')),
		("((a) ~b)", (o, o, 'a', c, d, uOr, 'b', c)),
		("~(a b)", (uOr, o, 'a', d, 'b', c)),

		("~a and ~b", (uOr, 'a', d, kwAnd, d, uOr, 'b')),
		("~a or ~b", (uOr, 'a', d, kwOr, d, uOr, 'b')),
		("~(-a) or ~(-b)", (uOr, o, uNot, 'a', c, d, kwOr, d, uOr, o, uNot, 'b', c)),

		("~(a) ~(b)", (uOr, o, 'a', c, d, uOr, o, 'b', c)),
		("(~a) (~b)", (o, uOr, 'a', c, d, o, uOr, 'b', c)),

		("(~a) ~b ~c", (o, uOr, 'a', c, d, uOr, 'b', d, uOr, 'c')),
		("~a (~b ~c)", (uOr, 'a', d, o, uOr, 'b', d, uOr, 'c', c)),

		("~a ~b or ~c ~d", (uOr, 'a', d, uOr, 'b', d, kwOr, d, uOr, 'c', d, uOr, 'd')),
		("~a ~b and ~c ~d", (uOr, 'a', d, uOr, 'b', d, kwAnd, d, uOr, 'c', d, uOr, 'd')),
		("(~a ~b) (~c ~d)", (o, uOr, 'a', d, uOr, 'b', c, d, o, uOr, 'c', d, uOr, 'd', c)),
		("~(a b) ~(c d)", (uOr, o, 'a', d, 'b', c, d, uOr, o, 'c', d, 'd', c)),
		("(a b) or (c d)", (o, 'a', d, 'b', c, d, kwOr, d, o, 'c', d, 'd', c)),

		(" a b c d", (d, 'a', d, 'b', d, 'c', d, 'd')),
		(" a b c ~d", (d, 'a', d, 'b', d, 'c', d, uOr, 'd')),
		(" a b ~c d", (d, 'a', d, 'b', d, uOr, 'c', d, 'd')),
		(" a b ~c ~d", (d, 'a', d, 'b', d, uOr, 'c', d, uOr, 'd')),
		(" a ~b c d", (d, 'a', d, uOr, 'b', d, 'c', d, 'd')),
		(" a ~b c ~d", (d, 'a', d, uOr, 'b', d, 'c', d, uOr, 'd')),
		(" a ~b ~c d", (d, 'a', d, uOr, 'b', d, uOr, 'c', d, 'd')),
		(" a ~b ~c ~d", (d, 'a', d, uOr, 'b', d, uOr, 'c', d, uOr, 'd')),
		("~a b c d", (uOr, 'a', d, 'b', d, 'c', d, 'd')),
		("~a b c ~d", (uOr, 'a', d, 'b', d, 'c', d, uOr, 'd')),
		("~a b ~c d", (uOr, 'a', d, 'b', d, uOr, 'c', d, 'd')),
		("~a b ~c ~d", (uOr, 'a', d, 'b', d, uOr, 'c', d, uOr, 'd')),
		("~a ~b c d", (uOr, 'a', d, uOr, 'b', d, 'c', d, 'd')),
		("~a ~b c ~d", (uOr, 'a', d, uOr, 'b', d, 'c', d, uOr, 'd')),
		("~a ~b ~c d", (uOr, 'a', d, uOr, 'b', d, uOr, 'c', d, 'd')),
		("~a ~b ~c ~d", (uOr, 'a', d, uOr, 'b', d, uOr, 'c', d, uOr, 'd')),

		# NOT queries:
	
		("-a", (uNot, 'a')),

		("(a -b)", (o, 'a', d, uNot, 'b', c)),
		("a (-b)", ('a', d, o, uNot, 'b', c)),
		("((a) -b)", (o, o, 'a', c, d, uNot, 'b', c)),


		# double negations:

		("-(-(-a))", (uNot, o, uNot, o, uNot, 'a', c, c)),

		("-(-a)", (uNot, o, uNot, 'a', c)),
		("-(-(-(-a)))", (uNot, o, uNot, o, uNot, o, uNot, 'a', c, c, c)),

		("a -(-(b)) c", ('a', d, uNot, o, uNot, o, 'b', c, c, d, 'c')),
		("a -(-(b -(-c))) d", ('a', d, uNot, o, uNot, o, 'b', d, uNot, o, uNot, 'c', c, c, c, d, 'd')),

		# DeMorgan's law:

		("-(a b)", (uNot, o, 'a', d, 'b', c)),
		("-(a or b)", (uNot, o, 'a', d, kwOr, d, 'b', c)),

		("-(a b c)", (uNot, o, 'a', d, 'b', d, 'c', c)),
		("-(a or b or c)", (uNot, o, 'a', d, kwOr, d, 'b', d, kwOr, d, 'c', c)),

		("-(-a -b -c)", (uNot, o, uNot, 'a', d, uNot, 'b', d, uNot, 'c', c)),
		("-(-a or -b or -c)", (uNot, o, uNot, 'a', d, kwOr, d, uNot, 'b', d, kwOr, d, uNot, 'c', c)),

		("-(a -(b -(c d)))", (uNot, o, 'a', d, uNot, o, 'b', d, uNot, o, 'c', d, 'd', c, c, c)),

		# distributive law:

		("a or (b c)", ('a', d, kwOr, d, o, 'b', d, 'c', c)),
		("(b c) or a", (o, 'b', d, 'c', c, d, kwOr, d, 'a')),

		("(a b) or (c d)", (o, 'a', d, 'b', c, d, kwOr, d, o, 'c', d, 'd', c)),

		("(a b) or (c d) or (e f)", (o, 'a', d, 'b', c, d, kwOr, d, o, 'c', d, 'd', c, d, kwOr, d, o, 'e', d, 'f', c)),

		# syntax errors:

		("(", (o,)),
		(",)", (',)',)),
		("-", (uNot,)),
		("~", (uOr,)),

		("(a", (o, 'a')),
		(",)a", (",)a",)),
		("-~a", (uNot, '~a')),
		("~-a", (uOr, '-a')),
		("~~a", (uOr, '~a')),
		("--a", (uNot, '-a')),

		("and", (kwAnd,)),
		("-and", (uNot, 'and')),
		("~and", (uOr, 'and')),
		("or", (kwOr,)),
		("-or", (uNot, 'or')),
		("~or", (uOr, 'or')),
		("a and", ('a', d, kwAnd)),
		("a or", ('a', d, kwOr)),
		('and a', (kwAnd, d, 'a')),
		('or a', (kwOr, d, 'a')),

		("a -", ('a', d, uNot)),
		("a ~", ('a', d, uOr)),

		("(a b", (o, 'a', d, 'b')),
		("(a (b)", (o, 'a', d, o, 'b', c)),

		('source:"foo', (m('source:'), dquote, 'foo')),
		('source:"foo bar', (m('source:'), dquote, 'foo bar')),

		# meta-prefixes with wildcards:
		
		('( source:a)*', (o, d, m('source:'), 'a)*')),
		('( source:a)*)', (o, d, m('source:'), 'a)*', c)),
		('( source:a)*))', (o, d, m('source:'), 'a)*)', c)),

		# unbalanced-exemption tags:

		('( :) )', (o, d, ':)', d, c)),
		('( :( )', (o, d, ':(', d, c)),
		('( ;) )', (o, d, ';)', d, c)),
		('( ;( )', (o, d, ';(', d, c)),
		('( >:) )', (o, d, '>:)', d, c)),
		('( >:( )', (o, d, '>:(', d, c)),

		('( -:) )', (o, d, uNot, ':)', d, c)),
		('( -:( )', (o, d, uNot, ':(', d, c)),
		('( -;) )', (o, d, uNot, ';)', d, c)),
		('( -;( )', (o, d, uNot, ';(', d, c)),
		('( ->:) )', (o, d, uNot, '>:)', d, c)),
		('( ->:( )', (o, d, uNot, '>:(', d, c)),

		('>:()', ('>:()',)),
		('>:())', ('>:())',)),
		('(>:()', (o, '>:()')),
		('(->:()', (o, uNot, '>:()')),
		('( >:()', (o, d, '>:()')),
		('( >:())', (o, d, '>:()', c)),
		('( >:()))', (o, d, '>:())', c)),
		('( ->:()))', (o, d, uNot, '>:())', c)),

		('>:))', ('>:))',)),
		('>:)))', ('>:)))',)),
		('(>:))', (o, '>:)', c)),
		('( >:))', (o, d, '>:)', c)),
		('( >:)))', (o, d, '>:))', c)),
		('( >:))))', (o, d, '>:)))', c)),
		('( ->:))', (o, d, uNot, '>:)', c)),
		('( ->:)))', (o, d, uNot, '>:))', c)),

		# invalid tag names:

		("___", ('___',)),
		("`foo", ('`foo',)),
		("%foo", ('%foo',)),
		("(foo", (o, 'foo')),
		(")foo", (c, 'foo')),
		("{foo", ('{foo',)),
		("}foo", ('}foo',)),
		("]foo", (']foo',)),
		("_foo", ('_foo',)),
		("foo_", ('foo_',)),
		("foo__bar", ('foo__bar',)),
		("foo,bar", ('foo,bar',)),
		("foo\abar", ('foo\abar',)),
		("café", ('café',)),
		("東方", ('東方',)),
		("X" * 171, ("X" * 171,)),
		("foo(", ('foo(',)),
		("foo)(", ('foo)(',)),
		("foo(()", ('foo(()',)),
		("foo())", ('foo())',)),

		# escapes:

		('source:a\\b', (m('source:'), 'a\\b')),
		('source:a\\\\b', (m('source:'), 'a\\\\b')),
		('source:\\', (m('source:'), '\\')),
		('source:\\"', (m('source:'), '\\"')),
		('source:\\*', (m('source:'), '\\*')),
		('source:\\\\', (m('source:'), '\\\\')),
		('source:"\\"\\""', (m('source:'), dquote, esc, '"', esc, '"', dquote)),
		('source:"\\"*\\""', (m('source:'), dquote, esc, '"*', esc, '"', dquote)),
		
		# https://github.com/danbooru/danbooru/issues/5286
		('source:\\*\\ \\*', (m('source:'), '\\*\\', d, '\\', wild)),
		('source:"\* \*"', (m('source:'), dquote, '\\* \\*', dquote)),

		# operators and wildcards:

		('*-', (wild, '-')),
		('*~', (wild, '~')),

		('*and', (wild, 'and')),
		('*or', (wild, 'or')),

		('and)', ('and)',)),
		('or)', ('or)',)),

		('(and)', (o, 'and', c)),
		('(or)', (o, 'or', c)),

		('( and)', (o, d, 'and', c)),
		('( or)', (o, d, 'or', c)),

		('(and', (o, kwAnd)),
		('(or', (o, kwOr)),

		('and(', ('and(',)),
		('or(', ('or(',)),

		('and(and', ('and(and',)),
		('or(or', ('or(or',)),

		('and)and', ('and)and',)),
		('or)or', ('or)or',)),

		('(a b)and (c d)', (o, 'a', d, 'b)and', d, o, 'c', d, 'd', c)),
		('(a b) and(c d)', (o, 'a', d, 'b', c, d, 'and(c', d, 'd)')),

		('(b)and a', (o, 'b)and', d, 'a')),
		('(b)or a', (o, 'b)or', d, 'a')),
		('(b )and a', (o, 'b', d, c, kwAnd, d, 'a')),
		('(b )or a', (o, 'b', d, c, kwOr, d, 'a')),

		('(source:', (o, m('source:'))),
		('(source:()', (o, m('source:'), '(', c)),
		('source:()', (m('source:'), '()')),
		('source:)', (m('source:'), ')')),
		('(source:")"', (o, m('source:'), dquote, ')', dquote)),
		('(source:)and a )', (o, m('source:'), ')and', d, 'a', d, c)),

		('(source:"s")a b', (o, m('source:'), dquote, 's', dquote, c, 'a', d, 'b')),
		('(source:"s")a b)', (o, m('source:'), dquote, 's', dquote, c, 'a', d, 'b)')),
		('(source:"s")and a', (o, m('source:'), dquote, 's', dquote, c, kwAnd, d, 'a')),
		('(source:"s")or a', (o, m('source:'), dquote, 's', dquote, c, kwOr, d, 'a')),
		('source:"s"and a', (m('source:'), dquote, 's', dquote, kwAnd, d, 'a')),
		('source:"s"or a', (m('source:'), dquote, 's', dquote, kwOr, d, 'a')),

		('-)', (uNot, c)),
		('~)', (uOr, c)),
		('-)a', (uNot, c, 'a')),
		('~)a', (uOr, c, 'a')),

		(')-and', (c, uNot, 'and')),
		(')-or', (c, uNot, 'or')),
		(')~and', (c, uOr, 'and')),
		(')~or', (c, uOr, 'or')),

		('a)-and', ('a)-and',)),
		('a)-or', ('a)-or',)),
		('a)~and', ('a)~and',)),
		('a)~or', ('a)~or',)),

		('(a)-b', (o, 'a)-b')),
		('(a)~b', (o, 'a)~b')),
		('(a)-b)', (o, 'a)-b', c)),
		('(a)~b)', (o, 'a)~b', c)),
		('(a )-b', (o, 'a', d, c, uNot, 'b')),
		('(a )~b', (o, 'a', d, c, uOr, 'b')),
		('(a )-b)', (o, 'a', d, c, uNot, 'b)')),
		('(a )~b)', (o, 'a', d, c, uOr, 'b)')),

		# adjacent parentheses:

		('( a )b', (o, d, 'a', d, c, 'b')),
		('( a )source:b', (o, d, 'a', d, c, m('source:'), 'b')),
		('( a)b', (o, d, 'a)b')),
		('( -)b', (o, d, uNot, c, 'b')),
		('( a )( b )', (o, d, 'a', d, c, o, d, 'b', d,  c)),
		('(a )( b)', (o, 'a', d, c, o, d, 'b', c)),
		('(a)(b)', (o, 'a)(b', c)),
		('( a)(b)', (o, d, 'a)(b', c)),
		('(a)( b)', (o, 'a)(', d, 'b', c)),
		('(a )(b)', (o, 'a', d, c, o, 'b', c)),
		('( a)()(*( )', (o, d, 'a)()(', wild, '(', d, c)),
		('(( a ) )', (o, o, d, 'a', d, c, d, c)),
		('( a*)(b)', (o, d, 'a', wild, ')(b', c)),
		('( a)*(b)', (o, d, 'a)', wild, '(b', c)),
		('( a)(*b)', (o, d, 'a)(', wild, 'b', c)),

		# abbreviations:

		('/', (abbrev,)),
		('/a', (abbrev, 'a')),
		('//', (abbrev, '/')),
		('//a', (abbrev, '/a')),
		('-/a', (uNot, abbrev, 'a')),
		('/-a', (abbrev, '-a')),
		('/*', (abbrev, wild)),
		('/a*', (abbrev, 'a', wild)),
		('/*a', (abbrev, wild, 'a')),
		('/)', (abbrev, ')')),
		('/(', (abbrev, '(')),
		('(/)', (o, abbrev, c)),
		('( /a))', (o, d, abbrev, 'a)', c)),
		('( /(a*)', (o, d, abbrev, '(a', wild, ')')),
		('/source:', (abbrev, 'source:')),
		('/source:a', (abbrev, 'source:a')),

		# non-ascii:

		('\u00e6', ('\u00e6',)),
		('\u00e6*', ('\u00e6*',)),
		('*\u00e6', ('*\u00e6',)),
		('a\u00e6', ('a\u00e6',)),
		('a*\u00e6', ('a*\u00e6',)),
		('-/a\u00e6', (uNot, '/a\u00e6',)),
		('-\u00e6', (uNot, '\u00e6',)),
		('/a\u00e6', ('/a\u00e6',)),
		('/a\u00e6*', ('/a\u00e6*',)),
		('/a*\u00e6', ('/a*\u00e6',)),
		('/*a\u00e6', ('/*a\u00e6',)),
		('-/a\u00e6', (uNot, '/a\u00e6',)),
		('\u0080*', ('\u0080*',)),
		('\U0010ffff*', ('\U0010ffff*',)),
		('\u017fource:', ('\u017fource:',)),

		# tags containing various characters:

		*((s, (s,)) for s in (
			'very_long_hair',
			'k-on!',
			'.hack//',
			'nyoro~n',
			're:zero',
			'#compass',
			'.hack//g.u.',
			'me!me!me!',
			'd.gray-man',
			'steins;gate',
			'tiger_&_bunny',
			'ssss.gridman',
			"yu-gi-oh!_5d's",
			'don\'t_say_"lazy"',
			"jack-o'-lantern",
			'd.va_(overwatch)',
			'rosario+vampire',
			"girls'_frontline",
			'fate/grand_order',
			'yorha_no._2_type_b',
			'love_live!_sunshine!!',
			"jeanne_d'arc_alter_(ver._shinjuku_1999)_(fate)",
			'kaguya-sama_wa_kokurasetai_~tensai-tachi_no_renai_zunousen~',
			':o',
			'o_o',
			'^_^',
			'^^^',
			'c.c.',
			'\\||/',
			'\\(^o^)/',
			'<o>_<o>',
			'<|>_<|>',
			'k-----s',
			'm.u.g.e.n',)),
	)

def _exprToIdent(s) -> str:
	'''
	Substitutes all non-lowercase characters with uppercase hex-digit-pairs.
	``don't_say_"lazy"`` -> ``don27t5Fsay5F22lazy22``.
	'''
	return re.sub(b'[^a-z]',
		lambda m: f'{ord(m[0]):02X}'.encode('utf-8'),
		s.encode('utf-8')).decode('utf-8')

def _assertExprTokens(self, expr: str, expectedToks, mode) -> None:
	actualToks = tuple(taglex.tagExprTokens(expr, mode))

	actualSimplified = tuple(map(_simplifyTok, actualToks))

	self.assertTupleEqual(actualSimplified, expectedToks,
		msg=f'''Should correctly tokenise expression - {pformat(expr)}''')

	self.assertEqual(''.join(t.value for t in actualToks), expr,
		msg='Tokens should concatenate back to the original expression.')

TagLexEditModeAutoTest = type('TagLexEditModeAutoTest', (TestCase,),
	{f'testTagExprEditModeTokens_{_exprToIdent(expr)}':
		partialmethod(_assertExprTokens, expr, expect, 'edit')
		for expr, expect in _exprsAndTokensForEditModeTests()})

TagLexQueryModeAutoTest = type('TagLexQueryModeAutoTest', (TestCase,),
	{f'testTagExprQueryModeTokens_{_exprToIdent(expr)}':
		partialmethod(_assertExprTokens, expr, expect, 'query')
		for expr, expect in _exprsAndTokensForQueryModeTests()})

def _simplifyTok(tok):
	if tok.type == 'literal':
		return tok.value
	return tok._replace(start=0, end=0)

_allSpaceChrs = ''.join(map(chr, (
	0x9, # tab
	0xa, # lf
	0xb, # vtab
	0xc, # ff
	0xd, # cr
	0x20, # space
	0x85, # nel
	0xa0, # nbsp
	0x1680, # ogham space mark
	0x2000, # en quad
	0x2001, # em quad
	0x2002, # en space
	0x2003, # em space
	0x2004, # three-per-em space
	0x2005, # four-per-em space
	0x2006, # six-per-em space
	0x2007, # figure space
	0x2008, # punctuation space
	0x2009, # thin space
	0x200a, # hair space
	0x2028, # line separator
	0x2029, # paragraph separator
	0x202f, # narrow nbsp
	0x205f, # medium mathematical space
	0x3000, # ideographic space
)))
